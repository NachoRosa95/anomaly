﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeBody : MonoBehaviour
{
    public List<TimePoint> TimePoints = new List<TimePoint>();
    // public List<Vector3> AppliedForces = new List<Vector3>();
    public Vector3 AppliedForce = Vector3.zero;
    public bool IsRecording = true;

    [SerializeField] bool debug;
    [SerializeField] float rewindSpeedMultiplier = 10000;
    [SerializeField] GameObject desiredStatesParent;
    [SerializeField] GameObject previousStatesParent;

    private List<GameObject> desiredStates = new List<GameObject>();
    private List<GameObject> previousStates = new List<GameObject>();
    private Rigidbody rb;
    private TimeBodyManager manager;
    private float t;
    private int rewindIndex;
    private float angularDrag;
    private bool useGravity;
    private bool isKinematic;
    private Quaternion lastRotation;

    public struct TimePoint
    {
        public Vector3 Position;
        public Quaternion Rotation;
        public float Time;
    }

    public struct TimePointRB
    {
        public Vector3 Velocity;
        public Vector3 AngularVelocity;
    }

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        useGravity = rb.useGravity;
        isKinematic = rb.isKinematic;
        angularDrag = rb.angularDrag;
        manager = TimeBodyManager.Instance;
        manager.TimeBodies.Add(this);
        if (desiredStatesParent)
            foreach (Transform child in desiredStatesParent.transform)
            {
                desiredStates.Add(child.gameObject);
            }
            
        if (previousStatesParent)
            foreach (Transform child in previousStatesParent.transform)
            {
                previousStates.Add(child.gameObject);
            }
    }

    void OnDestroy()
    {
        manager.TimeBodies.Remove(this);
    }

    public void RecordTimePoint()
    {
        if (!IsRecording) return;
        var timePoint = new TimePoint();
        timePoint.Position = transform.position;
        timePoint.Rotation = transform.rotation;
        timePoint.Time = Time.time;
        TimePoints.Add(timePoint);
        var firstPoint = TimePoints[0];
        if (timePoint.Time - firstPoint.Time > manager.MaxRecordTime)
        {
            TimePoints.Remove(firstPoint);
        }
        if (debug) Debug.Log(timePoint.Time - firstPoint.Time);
    }

    public float RewindTime(float time)
    {
        if(TimePoints.Count == 0) return 0;

        // Get the index recorded "time" seconds before the freeze, including decimals
        float rewindAmount = time / manager.RecordFrequency;
        rewindIndex = TimePoints.Count - 1 - (int)rewindAmount;
        rewindIndex = Mathf.Clamp(rewindIndex, 0, TimePoints.Count - 1);
        if (debug) Debug.Log(rewindIndex + " : " + (TimePoints.Count - 1));

        // Get the decimal component of rewindAmount (0 <= remainder < 1)
        float remainder = rewindAmount % 1;

        // Get the point at that index
        var timePoint = TimePoints[rewindIndex];

        // Initialize pos and rot
        var desiredPos = timePoint.Position;
        var desiredRotation = timePoint.Rotation;

        // If there's another data point before this one
        if (rewindIndex - 1 > 0)
        {
            // Get the point before this one
            var nextTimePoint = TimePoints[rewindIndex - 1];

            // Interpolate the position and rotation between this data point and the previous one using remainder as t
            desiredPos = Vector3.Lerp(desiredPos, nextTimePoint.Position, remainder);
            desiredRotation = Quaternion.Lerp(desiredRotation, nextTimePoint.Rotation, remainder);
        }

        // Difference between the desired position and the current one
        var diff = desiredPos - transform.position;

        // If desiredPos is further than some arbitrary min distance
        if (diff.magnitude > 0.01)
        {
            // The rigidbody is allowed to move
            rb.isKinematic = false;

            // Set its velocity to the difference times speed times deltaTime
            rb.velocity = diff * rewindSpeedMultiplier * Time.fixedDeltaTime;
        }
        // If desiredPos is close e5nough
        else
        {
            // The rigidbody is prevented from moving
            rb.isKinematic = true;

            // Set position to desired position to correct any errors
            rb.MovePosition(desiredPos);
        }

        rb.MoveRotation(desiredRotation);

        for (var i = 0; i < desiredStates.Count; i++)
        {
            var desiredState = desiredStates[i];
            var pos = Vector3.Lerp(transform.position, desiredPos, (i + 1) * 1f / desiredStates.Count);
            var rot = Quaternion.Lerp(transform.rotation, desiredRotation, (i + 1) * 1f / desiredStates.Count);
            desiredState.transform.position = pos;
            desiredState.transform.rotation = rot;
        }
        
        for (var i = 0; i < previousStates.Count; i++)
        {
            var previousState = previousStates[i];
            var index = Mathf.Lerp(0, TimePoints.Count, (float)i/previousStates.Count);
            var point = TimePoints[(int)index];
            var pos = point.Position;
            var rot = point.Rotation;
            previousState.transform.position = pos;
            previousState.transform.rotation = rot;
        }
        
        return (float) rewindIndex;
    }

    public void FreezeTime()
    {
        IsRecording = false;
        if(desiredStatesParent) desiredStatesParent.SetActive(true);
        if(previousStatesParent) previousStatesParent.SetActive(true);
        rb.useGravity = false;
        rb.angularDrag = 1;
    }

    public void ResumeTime()
    {
        if (IsRecording) return;
        if(desiredStatesParent) desiredStatesParent.SetActive(false);
        if(previousStatesParent) previousStatesParent.SetActive(false);
        ResetPhysics();
        // ApplyForces();

        IsRecording = true;
        TimePoints = new List<TimePoint>();
        ApplyForce();
    }

    private void ResetPhysics()
    {
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        rb.useGravity = useGravity;
        rb.isKinematic = isKinematic;
        rb.angularDrag = angularDrag;
    }

    // public void ApplyForces()
    // {
    //     if (IsRecording || AppliedForces.Count == 0) return;
    //     Vector3 totalForce = Vector3.zero;
    //     foreach(Vector3 appliedForce in AppliedForces)
    //     {
    //         totalForce += appliedForce;
    //     }
    //     rb.AddForce(totalForce, ForceMode.Impulse);
    //     AppliedForces.Clear();
    // }

    public void ApplyForce()
    {
        if (!IsRecording) { return; }
        
        rb.AddForce(AppliedForce, ForceMode.Impulse);
        AppliedForce = Vector3.zero;
    }
}
