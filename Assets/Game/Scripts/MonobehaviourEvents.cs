﻿using UnityEngine;
using UnityEngine.Events;

public class MonobehaviourEvents : MonoBehaviour
{
    public UnityEvent OnAwakeEvent = new UnityEvent();
    public UnityEvent OnEnabledEvent = new UnityEvent();
    public UnityEvent OnDisabledEvent = new UnityEvent();

    void Awake()
    {
        OnAwakeEvent?.Invoke();
    }

    void OnEnable()
    {
        OnEnabledEvent?.Invoke();
    }

    void OnDisable()
    {
        OnDisabledEvent?.Invoke();
    }
}
