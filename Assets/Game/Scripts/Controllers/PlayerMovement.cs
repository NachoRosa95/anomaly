﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]

public class PlayerMovement : MonoBehaviour
{
    public Animator HandLeftAnimator;
    public Animator HandRightAnimator;
    public Animator BodyAnimator;

    // [SerializeField] LayerMask groundLayers;
    [SerializeField] Animator headAnimator;
    [SerializeField] CameraController cameraController;
    [SerializeField] float speed = 10.0f;
    [SerializeField] float sprintSpeed = 20.0f;
    [SerializeField] float gravity = 10.0f;
    [SerializeField] float maxVelocityChange = 10.0f;
    [SerializeField] float jumpHeight = 2.0f;
    [SerializeField] float maxSlope; // WIP
    [SerializeField] int maxJumps = 1;
    [SerializeField] float jumpCooldown = 0.2f;
    [SerializeField] float skin = 0.09f;
    [SerializeField] bool debugRayGrounded;

    [FoldoutGroup("Audio")]
    [SerializeField] AudioSource audioLand;
    [FoldoutGroup("Audio")]
    [SerializeField] List<AudioSource> audioJumps = new List<AudioSource>();
    [FoldoutGroup("Audio")]
    [SerializeField] List<AudioSource> audioFootsteps = new List<AudioSource>();

    private Rigidbody rb;
    private CapsuleCollider col;
    private Vector3 rayVector;
    private ContactPoint[] debugContacts = { };
    private Respawn respawn;
    private List<Renderer> renderers;
    private int curJumps;
    private bool grounded = false;
    private bool jumpedRecently = false;
    private bool noClipping = false;
    private bool frozen = false;

    public bool Grounded => grounded;
    public bool JumpedRecently => jumpedRecently;
    public bool Frozen => frozen;
    public CameraController CameraController => cameraController;

    private const float noClipSpeed = 5f;

    public void PlayFootstep()
    {
        var randomFootstep = audioFootsteps[Random.Range(0, audioFootsteps.Count - 1)];
        if (randomFootstep) randomFootstep.PlayOneShot(randomFootstep.clip);
    }

    public void PlayJump()
    {
        var randomJump = audioJumps[Random.Range(0, audioJumps.Count - 1)];
        if (randomJump) randomJump.PlayOneShot(randomJump.clip);
    }

    public void SetSpawnPoint(Transform reference)
    {
        respawn.SetSpawnPoint(reference);
    }

    public void Freeze(bool value)
    {
        frozen = value;
    }

    public void Hide(bool value)
    {
        foreach (var ren in renderers)
        {
            ren.enabled = !value;
        }
    }

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        respawn = GetComponent<Respawn>();
        rayVector = -Vector3.up * (col.bounds.extents.y + skin);
        renderers = new List<Renderer>(GetComponentsInChildren<Renderer>(true));
    }

    void FixedUpdate()
    {
        // debugRayGrounded = RayCastGrounded();
        // if (debugRayGrounded) SetGrounded();

        if (frozen)
        {
            cameraController.enabled = false;
            rb.velocity = Vector3.zero;
            headAnimator.enabled = false;
            return;
        }
        headAnimator.enabled = true;
        cameraController.enabled = true;

        // Calculate how fast it should be moving
        Vector3 targetVelocity = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        targetVelocity *= (Input.GetButton("Sprint") ? sprintSpeed : speed);
        targetVelocity *= noClipping ? noClipSpeed : 1;
        headAnimator.SetFloat("Speed", targetVelocity.magnitude);



        if (transform.parent != null)
        {
            // Using Transform.Translate instead of RigidBody on lateral movement allows for great compatibility with moving platforms
            // Downside is you can walk through colliders and can't walk up slopes
            // Solution is to use Translate only while on a platform
            transform.Translate(targetVelocity * Time.fixedDeltaTime);

            // Adds constraints to lateral movement so the rigidbody doesn't slide off the platform regardless of PhysicsMaterial
            rb.constraints |= RigidbodyConstraints.FreezePositionX;
            rb.constraints |= RigidbodyConstraints.FreezePositionZ;
        }
        else
        {
            // Set targetVelocity's frame of reference to this transform
            if (!noClipping) targetVelocity = transform.TransformDirection(targetVelocity);
            else targetVelocity = Camera.main.transform.TransformDirection(targetVelocity);

            // Debug
            var origin = transform.position + Vector3.up;
            Debug.DrawLine(origin, origin + targetVelocity, Color.blue);

            // Apply a force that attempts to reach target velocity
            Vector3 velocity = rb.velocity;
            Vector3 velocityChange = (targetVelocity - velocity);
            velocityChange.x = Mathf.Clamp(velocityChange.x, -maxVelocityChange, maxVelocityChange);
            velocityChange.z = Mathf.Clamp(velocityChange.z, -maxVelocityChange, maxVelocityChange);
            velocityChange.y *= noClipping ? 1 : 0;
            rb.AddForce(velocityChange, ForceMode.VelocityChange);

            // Removes constraints to lateral movement that may have been added by stepping on a platform
            rb.constraints &= ~RigidbodyConstraints.FreezePositionX;
            rb.constraints &= ~RigidbodyConstraints.FreezePositionZ;
        }


        if (grounded && curJumps < maxJumps && !noClipping)
        {
            // Jump
            if (!jumpedRecently && Input.GetButton("Jump"))
            {
                Debug.Log("Jump");
                PlayJump();
                // Setting the Rigidbody velocity directly makes for a very elegant, realistic and predictable jump
                rb.velocity = new Vector3(rb.velocity.x, CalculateJumpVerticalSpeed(), rb.velocity.z);
                curJumps++;
                grounded = false;
                jumpedRecently = true;
                StartCoroutine("doJumpCooldown");
                headAnimator.SetBool("Jumping", true);
            }
        }

        headAnimator.SetBool("Bobbing", grounded);


        // Apply gravity manually for more tuning control
        if (!noClipping) rb.AddForce(new Vector3(0, -gravity * rb.mass, 0));

        grounded = false;
    }

    void Update()
    {
        if (Input.GetButtonDown("NoClip"))
        {
            noClipping = !noClipping;
            Debug.Log("Noclip: " + noClipping);
            col.enabled = !noClipping;
            var velocity = rb.velocity;
            velocity.y = 0;
            rb.velocity = velocity;
        }
    }

    float CalculateJumpVerticalSpeed()
    {
        // From the jump height and gravity deduce the upwards speed 
        // for the character to reach at the apex
        return Mathf.Sqrt(2 * jumpHeight * gravity);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        foreach (ContactPoint point in debugContacts)
        {
            Gizmos.DrawLine(point.point, point.point + point.normal * 0.2f);
        }
        if (col && rayVector != null)
            Gizmos.DrawLine(col.bounds.center, col.bounds.center + rayVector);
    }

    IEnumerator doJumpCooldown()
    {
        // Coroutine that waits jumpCooldown seconds and then stops jumping
        yield return new WaitForSeconds(jumpCooldown);
        jumpedRecently = false;
    }

    void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Unwalkable") return;
        foreach (ContactPoint point in collision.contacts)
        {
            if (point.normal.y > 0.8f)
            {
                SetGrounded();
            }
        }
        debugContacts = collision.contacts;
    }

    void SetGrounded()
    {
        if (jumpedRecently) return;
        headAnimator.SetBool("Jumping", false);
        grounded = true;
        curJumps = 0;
    }

    // bool RayCastGrounded()
    // {
    //     RaycastHit hit;
    //     if (Physics.Raycast(col.bounds.center, rayVector, out hit, rayVector.magnitude, groundLayers))
    //     {
    //         return true;
    //     }
    //     return false;
    // }
}