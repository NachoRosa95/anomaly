﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonCamController : MonoBehaviour
{
    [SerializeField] Transform rootTransform;
    [SerializeField] float sensitivity = 80;
    // [SerializeField] float maxAngleX = 280f;
    // [SerializeField] float minAngleX = 80f;
    [SerializeField] bool invertY;
    Vector2 desiredRotation;

    void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        HandleInput();
        UpdateRotation();
    }

    void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0)) Cursor.lockState = CursorLockMode.Locked;

        // "Mouse X" and "Mouse Y" each refer to two axes mapped to mouse movement X & Y, and joystick axes 4 & 5 respectively
        // Thus this allows controller and mouse support in one
        var mouseX = Input.GetAxis("Mouse X");
        var mouseY = Input.GetAxis("Mouse Y");

        if (invertY) mouseY *= -1;

        desiredRotation = new Vector2(mouseX * Time.deltaTime * sensitivity, mouseY * Time.deltaTime * sensitivity);
    }

    void UpdateRotation()
    {
        transform.RotateAround(transform.position, transform.right, desiredRotation.y);
        if (rootTransform) rootTransform.RotateAround(transform.position, Vector3.up, desiredRotation.x);

        // var angles = transform.eulerAngles;
        // angles.x = ClampAngle(angles.x, minAngleX, maxAngleX);
        // angles.z = 0;
        // transform.eulerAngles = angles;
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        angle = Mathf.Repeat(angle, 360);
        min = Mathf.Repeat(min, 360);
        max = Mathf.Repeat(max, 360);
        bool inverse = false;
        var tmin = min;
        var tangle = angle;
        if (min > 180)
        {
            inverse = !inverse;
            tmin -= 180;
        }
        if (angle > 180)
        {
            inverse = !inverse;
            tangle -= 180;
        }
        var result = !inverse ? tangle > tmin : tangle < tmin;
        if (!result)
            angle = min;

        inverse = false;
        tangle = angle;
        var tmax = max;
        if (angle > 180)
        {
            inverse = !inverse;
            tangle -= 180;
        }
        if (max > 180)
        {
            inverse = !inverse;
            tmax -= 180;
        }

        result = !inverse ? tangle < tmax : tangle > tmax;
        if (!result)
            angle = max;
        return angle;
    }
}
