﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] GameObject head;
    [SerializeField] CharacterController _char;
    [SerializeField] Wobble wobble;
    [SerializeField] LayerMask layerMask;
    [SerializeField] float speed = 5;
    [SerializeField] float sprintSpeed = 10;
    [SerializeField] float sprintFOVChange = 15;
    [SerializeField] float jumpSpeed = 10;
    [SerializeField] float jumpCooldown = 0.2f;
    [SerializeField] float gravity = 1;
    [SerializeField] int maxJumps;
    [SerializeField] bool isGrounded;
    [SerializeField] bool charControllerGrounded;
    [SerializeField] bool drawVelocity;

    List<Vector3> rayOffsets = new List<Vector3>();
    Vector3 relativeVelocity;
    Vector3 velocity;
    Camera cam;
    float walkFOV;
    float rayDist;
    bool jumping;
    int curJumps;

    const float TerminalSpeed = 40;

    void Start()
    {
        if (!_char) _char = GetComponent<CharacterController>();
        cam = Camera.main;
        walkFOV = cam.fieldOfView;
        var raySkin = 0;
        rayDist = _char.bounds.extents.y + _char.skinWidth + raySkin;
    }

    void Update()
    {
        Gravity();
        HandleInput();
        UpdateChar();
        isGrounded = IsGrounded();
    }

    void HandleInput()
    {
        var x = Input.GetAxisRaw("Horizontal");
        var z = Input.GetAxisRaw("Vertical");

        var _speed = Input.GetKey(KeyCode.LeftShift) ? sprintSpeed : speed;
        var desiredFOV = Input.GetKey(KeyCode.LeftShift) && z > 0 ? walkFOV + sprintFOVChange : walkFOV;
        cam.fieldOfView = Mathf.MoveTowards(cam.fieldOfView, desiredFOV, 100 * Time.deltaTime);

        var horizontalVelocity = new Vector3(x, 0, z);
        horizontalVelocity = horizontalVelocity.normalized * _speed;

        relativeVelocity = new Vector3(horizontalVelocity.x, relativeVelocity.y, horizontalVelocity.z);

        if (curJumps < maxJumps && Input.GetKeyDown(KeyCode.Space))
        {
            jumping = true;
            StartCoroutine("doJumpCooldown");
            relativeVelocity.y = jumpSpeed;
            curJumps++;
        }
    }

    void Gravity()
    {
        if (!isGrounded)
        {
            relativeVelocity.y -= gravity * Time.deltaTime;
            if (relativeVelocity.y < -TerminalSpeed) relativeVelocity.y = -TerminalSpeed;
            wobble.GlobalSpeedMultiplier = 0;
        }
        else
        {
            curJumps = 0;
            relativeVelocity.y = -gravity * Time.deltaTime;

            var wobbleVelocity = relativeVelocity;
            wobbleVelocity.y = 0;
            wobble.GlobalSpeedMultiplier = wobbleVelocity.magnitude;
        }
    }

    void UpdateChar()
    {

        Vector3 desiredForward = head.transform.forward;
        desiredForward.y = 0;

        // To rotate velocity to match desiredForward:
        // Get angle between desiredForward and world forward (always positive)
        var angle = Vector3.Angle(desiredForward, Vector3.forward);
        // Get cross product between desiredForward and world forward
        var cross = Vector3.Cross(desiredForward, Vector3.forward);
        // The angle should be negative when the y component of cross product is positive and viceversa
        angle = cross.y > 0 ? -angle : angle;
        // Apply the angle to velocity using quaternion multiplication
        velocity = Quaternion.AngleAxis(angle, Vector3.up) * relativeVelocity;

        // velocity.z += 0.01f;
        Debug.DrawLine(transform.position, transform.position + Vector3.up, Color.magenta);
        Debug.Log(velocity);
        // _char.Move(velocity * Time.deltaTime);
        _char.SimpleMove(velocity);
        _char.Move(Vector3.up * velocity.y * Time.deltaTime);
        Debug.DrawLine(transform.position, transform.position + Vector3.up, Color.blue);
    }

    // Objective: prevent the controller from falling through colliders by casting a ray from its center to its bottom
    bool IsGrounded()
    {
        // If the controller returns grounded there's no issues
        charControllerGrounded = _char.isGrounded;
        if (jumping) return false;
        // if (_char.isGrounded) return true;

        // If not check to see if there's something between the center of the controller and its bottom
        var rayCastGrounded = false;

        // - - - To cast a ray from the center of the collider:
        rayOffsets.Add(Vector3.zero);

        // - - - To cast <steps> rays along the edges of the collider:
        // int steps = 8;
        // if (rayOffsets.Count == 0)
        // {
        //     // Draw a cylinder with a <steps>-sided polygon as its base
        //     for (int i = 0; i < steps; i++)
        //     {
        //         Vector3 offset = Quaternion.AngleAxis(360 / steps * i, Vector3.up) * Vector3.forward * _char.bounds.extents.x / 2;
        //         rayOffsets.Add(offset);
        //     }
        // }

        // The logic of each ray, it stops when one of them returns true
        foreach (Vector3 offset in rayOffsets)
        {
            var pos = transform.position + offset;
            RaycastHit hit;
            if (Physics.Raycast(transform.position, -Vector3.up * rayDist, out hit, rayDist, layerMask))
            {
                rayCastGrounded = true;
                break;
            }
        }
        // Debug.Log("Finished casting with result "+rayCastGrounded);

        return rayCastGrounded;
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, 0.2f);
        Gizmos.color = Color.blue;
        Vector3 origin = transform.position;
        var arrowWidth = 2;
        var arrowHeight = 2 + velocity.magnitude;
        if (drawVelocity) DrawArrow(origin, velocity, arrowWidth, arrowHeight, -arrowHeight / 2, Color.yellow);

        if (_char.isGrounded) return;
        Gizmos.color = Color.red;
        foreach (Vector3 offset in rayOffsets)
        {
            var pos = transform.position + offset;
        }
    }

    void DrawArrow(Vector3 pos, Vector3 dir, float width, float height, float heightOffset, Color color = default(Color))
    {
        Vector3 forward = dir.normalized;
        Vector3 right = Vector3.Cross(dir, Vector3.up).normalized;
        var origin = pos + forward * heightOffset;
        var pointA = origin + right * -width / 2;
        var pointB = origin + right * width / 2;
        var pointC = origin + forward * height;
        Gizmos.color = color;
        Gizmos.DrawLine(pointA, pointB);
        Gizmos.DrawLine(pointB, pointC);
        Gizmos.DrawLine(pointA, pointC);
    }

    IEnumerator doJumpCooldown()
    {
        yield return new WaitForSeconds(jumpCooldown);
        jumping = false;
    }
}
