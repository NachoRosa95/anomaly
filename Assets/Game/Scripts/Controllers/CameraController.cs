﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] float sensitivity = 3;
    [SerializeField] Transform rootTransform;
    [SerializeField] Transform cameraPivot;
    [SerializeField] Vector2 clampX;

    private Vector2 rotation = Vector2.zero;
    // private const float maxDelta = 15;

    public Transform CameraPivot => cameraPivot;

    void Awake()
    {
        if (rootTransform) rotation = rootTransform.eulerAngles;
        if (cameraPivot)
        {
            var cam = Camera.main;
            // if (cam) cam.transform.SetParent(cameraPivot, worldPositionStays: true);
        }
    }

    void FixedUpdate()
    {
        if (!Application.isEditor || Application.isEditor && Input.GetKeyDown(KeyCode.Mouse0))
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        // "Mouse X" and "Mouse Y" each refer to two axes mapped to mouse movement X & Y, and joystick axes 4 & 5 respectively
        // Thus this allows controller and mouse support in one

        // SET ROTATION
        // rotation.y += Input.GetAxis("Mouse X") * sensitivity * Time.fixedDeltaTime;
        // rotation.x += Input.GetAxis("Mouse Y") * sensitivity * Time.fixedDeltaTime;

        // rotation.x = Mathf.Clamp(rotation.x, -90f, 90f);
        // transform.localRotation = Quaternion.Euler(rotation.x, 0, 0);

        // ADD ROTATION
        var deltaY = Input.GetAxis("Mouse X") * sensitivity * Time.fixedDeltaTime;
        var deltaX = Input.GetAxis("Mouse Y") * sensitivity * Time.fixedDeltaTime;

        // deltaX = Mathf.Clamp(deltaX, -maxDelta, maxDelta);

        // if (deltaX < 0 && transform.localRotation.eulerAngles.x < -90f) deltaX = 0;[
        // if (deltaX > 0 && transform.localRotation.eulerAngles.x > 90f) deltaX = 0;]

        Vector3 rot = transform.localRotation.eulerAngles + new Vector3(deltaX, 0f, 0f);
        rot.x = Utils.ClampAngle(rot.x, clampX.x, clampX.y);
        rot.y = 0;
        rot.z = 0;

        transform.localRotation = Quaternion.Euler(rot);

        // transform.Rotate(0, deltaY, 0, Space.World);
        // transform.Rotate(deltaX, 0, 0, Space.Self);

        if (rootTransform) rootTransform.eulerAngles += new Vector3(0, deltaY, 0);
    }

}