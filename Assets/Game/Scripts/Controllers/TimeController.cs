﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeController : MonoBehaviour
{
    [SerializeField] Haulable haulable;
    [SerializeField] Animator rewindAnim;
    [SerializeField] ParticleSystem rewindParticleTrail;
    [SerializeField] TimeBody timeBody;
    [SerializeField] GameObject highlight;
    [SerializeField] bool paused;
    [SerializeField] bool debug;

    public float RewindTime;
    public bool Paused
    {
        get => paused;
        set
        {
            Highlighted = value;
            if (paused == value) return;

            if (value)
            {
                rewindParticleTrail.Pause(false);
                if (rewindAnim && rewindAnim.isActiveAndEnabled) rewindAnim.SetBool("Active", true);
                Debug.Log("Freezing Time", this);
                timeBody.FreezeTime();
            }

            if (!value)
            {
                rewindParticleTrail.Clear();
                rewindParticleTrail.Play();
                var em = rewindParticleTrail.emission;
                em.enabled = true;
                if (rewindAnim && rewindAnim.isActiveAndEnabled) rewindAnim.SetBool("Active", false);
                timeBody.ResumeTime();
            }

            paused = value;
        }
    }
    public bool Highlighted;

    SpellBook spellBook;
    TimeBodyManager manager;
    ParticleSystem.Particle[] m_Particles;
    ParticleSystem.Particle debugParticle;
    // int hideLayer = 9;
    int initLayer;
    int rewindIndex;
    bool wasPaused = true;
    // bool wasHaulable = false;

    void Awake()
    {
        if (!timeBody) timeBody = GetComponent<TimeBody>();
        if (!timeBody) Debug.LogError("No TimeBody attached to this GameObject", this);
        manager = TimeBodyManager.Instance;

        if (m_Particles == null || m_Particles.Length < rewindParticleTrail.main.maxParticles)
            m_Particles = new ParticleSystem.Particle[rewindParticleTrail.main.maxParticles];

        if (rewindParticleTrail)
        {
            var main = rewindParticleTrail.main;
            main.startLifetime = manager.MaxRecordTime;
            initLayer = rewindParticleTrail.gameObject.layer;
        }
        // Utils.SetLayerRecursively(rewindParticleTrail.transform, hideLayer);
        // if(haulable) wasHaulable = haulable.enabled;
    }

    void Start()
    {
        spellBook = SpellBook.Instance;
        paused = !paused;
        Paused = !paused;
    }

    public void TogglePause()
    {
        Paused = !Paused;
        wasPaused = Paused;
    }

    void FixedUpdate()
    {
        if (rewindParticleTrail)
        {
            rewindParticleTrail.gameObject.SetActive(spellBook.RewindEnabled);
        }

        if (highlight)
        {
            highlight.SetActive(Highlighted);
        }

        if (timeBody.TimePoints.Count > 1)
        {
            var maxTime = timeBody.TimePoints[timeBody.TimePoints.Count - 1].Time - timeBody.TimePoints[0].Time;
            maxTime = Mathf.Abs(maxTime);
            RewindTime = Mathf.Clamp(RewindTime, 0, maxTime);
            if (rewindAnim && rewindAnim.isActiveAndEnabled)
            {
                rewindAnim.SetFloat("Time", RewindTime / maxTime);
            }
        }
        // if(debugParticle.) Debug.Log(debugParticle.remainingLifetime);
        if (Paused)
        {
            // Highlighted = true;
            // // haulable.enabled = false;
            // if (!wasPaused)
            // {
            //     rewindParticleTrail.Pause(false);
            //     if (rewindAnim && rewindAnim.isActiveAndEnabled) rewindAnim.SetBool("Active", true);
            //     // Utils.SetLayerRecursively(rewindParticleTrail.transform, initLayer);
            //     Debug.Log("Freezing Time", this);
            // }
            timeBody.FreezeTime();
            var outOfPoints = timeBody.RewindTime(RewindTime) < 1;
            if (rewindAnim && rewindAnim.isActiveAndEnabled) rewindAnim.SetBool("OutOfPoints", outOfPoints);

        }
        else
        {
            // Highlighted = false;
            // // haulable.enabled = wasHaulable;
            // if (wasPaused)
            // {
            //     rewindParticleTrail.Clear();
            //     rewindParticleTrail.Play();
            //     var em = rewindParticleTrail.emission;
            //     em.enabled = true;
            //     if (rewindAnim && rewindAnim.isActiveAndEnabled) rewindAnim.SetBool("Active", false);
            //     // Utils.SetLayerRecursively(rewindParticleTrail.transform, hideLayer);
            // }
            // timeBody.ResumeTime();
            RewindTime = 0;
        }
        wasPaused = Paused;
    }
}
