﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeBodyManager : MonoBehaviour
{
    public List<TimeBody> TimeBodies = new List<TimeBody>();

    private static TimeBodyManager instance;


    public float RecordFrequency = 0.1f;
    public float MaxRecordTime = 5f;
    public bool RewindEnabled;

    private List<TimeBody> nullBodies = new List<TimeBody>();
    private float t = 0;


    public static TimeBodyManager Instance
    {
        get
        {
            if (instance == null)
            {
                GameObject go = new GameObject("TimeBodyManager");
                instance = go.AddComponent<TimeBodyManager>();
            }

            return instance;
        }

        private set
        {
            instance = value;
        }
    }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Debug.LogError("Duplicated Singleton", this);
            return;
        }
        instance = this;
    }

    void FixedUpdate()
    {
        t += Time.deltaTime;
        if (t > RecordFrequency)
        {
            foreach (TimeBody timeBody in TimeBodies)
            {   
                timeBody.RecordTimePoint();
            }
            t %= RecordFrequency;
        }
    }

}
