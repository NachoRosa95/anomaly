﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsManager : MonoBehaviour
{
    public static StatsManager Instance
    {
        get
        {
            if (!instance)
            {
                var go = GameObject.Find("Managers") ?? new GameObject("Managers");
                instance = go.GetComponent<StatsManager>() ?? go.AddComponent<StatsManager>();
                DontDestroyOnLoad(go);
            }
            return instance;
        }
        set => instance = value;
    }

    private static StatsManager instance;
    private Dictionary<string, List<string>> statRegisters = new Dictionary<string, List<string>>();

    /// <summary>
    /// Registers the stat of given type and key, creating the register if it doesn't exist and avoiding duplicate keys.
    /// </summary>
    /// <param name="statType"></param>
    /// <param name="statKey"></param>
    public void RegisterStat(string statType, string statKey)
    {
        var statRegister = GetRegister(statType);
        if (statRegister.Contains(statKey)) return;
        statRegister.Add(statKey);
    }

    /// <summary>
    /// Returns a register of given type, creating one unless specified.
    /// </summary>
    /// <param name="statType"></param>
    /// <param name="create"></param>
    /// <returns></returns>
    public List<string> GetRegister(string statType, bool create = true)
    {
        if (!statRegisters.TryGetValue(statType, out var statRegister))
        {
            if (!create) return null;
            statRegister = new List<string>();
            statRegisters.Add(statType, statRegister);
        }
        return statRegister;
    }

    /// <summary>
    /// Clears all registers.
    /// </summary>
    public void Reset()
    {
        statRegisters.Clear();
    }
}
