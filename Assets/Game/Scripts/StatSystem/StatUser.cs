﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatUser : MonoBehaviour
{
    [SerializeField] string statType;
    [SerializeField] string statKey;

    public void RegisterStat()
    {
        if (statType == string.Empty || statKey == string.Empty)
        {
            Debug.LogError("StatUser is not configured", this);
        }
        var manager = StatsManager.Instance;
        manager.RegisterStat(statType, statKey);
    }
}
