﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StatLabel : MonoBehaviour
{
    [SerializeField] string statType;
    [SerializeField, TextArea] string content;
    [SerializeField] int maxCount = 6;
    List<string> statRegister;
    TextMeshProUGUI text;

    void Awake()
    {
        statRegister = StatsManager.Instance.GetRegister(statType);
        text = GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        text.text = $"{statRegister.Count} / {maxCount} " + content;
    }
}
