﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class OutlineController : MonoBehaviour
{
    [System.Serializable]
    public class OutlineData
    {
        public Color color = Color.white;
        public HighlightsFX.SortingType depthType;
        public GameObject root;
        public bool enabled;

        [HideInInspector]
        public Renderer[] renderers;
    }

    public HighlightsFX outlinePostEffect;
    public OutlineData[] outliners;
    public string CameraTag = "UICamera";

    private void Start()
    {
    }

    private void Update()
    {
        if (!outlinePostEffect)
        {
            var cam = GameObject.FindWithTag(CameraTag);
            if (cam) outlinePostEffect = cam.GetComponent<HighlightsFX>();
            if (!outlinePostEffect) return;
            foreach (var obj in outliners)
            {
                obj.renderers = obj.root.GetComponentsInChildren<Renderer>();

                // Filters out particle systems
                obj.renderers = obj.renderers.Where(rend => !(rend is ParticleSystemRenderer)).ToArray();
                outlinePostEffect.AddOutliner(obj);
            }
        }
    }

    private void OnEnable()
    {
        foreach (var obj in outliners)
        {
            obj.enabled = true;
        }
    }

    private void OnDisable()
    {
        foreach (var obj in outliners)
        {
            obj.enabled = false;
        }
    }
}
