﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class MatchParentCam : MonoBehaviour
{
    Camera parentCam;
    Camera cam;

    void Awake()
    {
        parentCam = transform.parent.GetComponent<Camera>();
        cam = GetComponent<Camera>();
    }

    void Update()
    {
        if (!parentCam || !cam) return;

        cam.fieldOfView = parentCam.fieldOfView;
    }
}
