﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

public class GateElement : MonoBehaviour
{
    public enum ElementState
    {
        Closed,
        Closing,
        Opening,
        Open
    }

    public UnityEvent OnOpened = new UnityEvent();
    public UnityEvent OnClosed = new UnityEvent();

    public ElementState State => state;
    public bool CanOpen => state == ElementState.Closed || state == ElementState.Closing;
    public bool CanClose => state == ElementState.Open || state == ElementState.Opening;

    [SerializeField] ElementState state;
    [SerializeField] float autoCloseTime = 0;
    [SerializeField] bool debug;

    private Animator animator;


    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    [Button("Open")]
    public void Open()
    {
        if (debug) Debug.Log("Staring open GateElement", this);
        if (!CanOpen) return;
        if (!isActiveAndEnabled) gameObject.SetActive(true);
        if (debug) Debug.Log("Opening GateElement", this);
        StartOpen();
    }

    [Button("Close")]
    public void Close()
    {
        if (debug) Debug.Log("Staring close GateElement", this);
        if (!CanClose) return;
        if (debug) Debug.Log("Closing GateElement", this);
        StartClose();
    }

    public void SetState(bool open)
    {
        if (open) Open();
        else Close();
    }

    public void ForceOpen()
    {
        state = ElementState.Open;
        gameObject.SetActive(true);
    }

    public void ForceClose()
    {
        state = ElementState.Closed;
        gameObject.SetActive(false);
    }

    // TODO: Replace SetActive with animator.SetTrigger
    private void StartOpen()
    {
        StopAllCoroutines();
        if (autoCloseTime > 0) StartCoroutine(AutoCloseCoroutine(autoCloseTime));
        if (Application.isPlaying) StartCoroutine(OpenCoroutine());
        else ForceOpen();
    }

    // TODO: Replace SetActive with animator.SetTrigger
    private void StartClose()
    {
        StopAllCoroutines();
        if (Application.isPlaying) StartCoroutine(CloseCoroutine());
        else ForceClose();
    }

    IEnumerator OpenCoroutine()
    {
        state = ElementState.Opening;
        if (!animator)
        {
            animator = GetComponent<Animator>();
            if (!animator)
            {
                state = ElementState.Open;
                gameObject.SetActive(true);
                OnOpened?.Invoke();
                yield break;
            }
        }
        if (debug)
        {
            Debug.LogWarning("Setting animator open", this);
        }
        animator.SetTrigger("Open");
        animator.SetBool("Open", true);
        yield return new WaitForSeconds(0.25f);
        yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.99f);
        OnOpened?.Invoke();
        state = ElementState.Open;
    }

    IEnumerator CloseCoroutine()
    {
        state = ElementState.Closing;
        if (!animator)
        {
            animator = GetComponent<Animator>();
            if (!animator)
            {
                state = ElementState.Closed;
                gameObject.SetActive(false);
                OnClosed?.Invoke();
                yield break;
            }
        }
        if (debug)
        {
            Debug.LogWarning("Setting animator close", this);
        }
        animator.SetTrigger("Close");
        animator.SetBool("Open", false);

        // Waits for the current animation to end
        var nameHash = animator.GetCurrentAnimatorStateInfo(0).shortNameHash;
        yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(0).shortNameHash != nameHash);

        // Waits for the closing animation to end
        nameHash = animator.GetCurrentAnimatorStateInfo(0).shortNameHash;
        yield return new WaitUntil(() => animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.99f);

        OnClosed?.Invoke();
        state = ElementState.Closed;
    }

    IEnumerator AutoCloseCoroutine(float duration)
    {
        yield return new WaitForSeconds(duration);
        Close();
    }
}
