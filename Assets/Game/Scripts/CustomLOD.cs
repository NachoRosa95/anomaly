﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class CustomLOD : MonoBehaviour
{
    [SerializeField] List<DistanceGroup> distanceGroups = new List<DistanceGroup>();
    [SerializeField] bool useCameraOffset = true;

    Camera mainCam;
    Vector3 cameraOffset;

    [System.Serializable]
    public class DistanceGroup
    {
        public List<GameObject> GameObjects;
        public float minDistance;
        public float maxDistance;
    }

    void Awake()
    {
        // mainCam = Camera.main;
        // cameraOffset = mainCam.transform.position - transform.position;
    }

    void Update()
    {
        if (!mainCam)
        {
            mainCam = Camera.main;
            cameraOffset = mainCam.transform.position - transform.position;
        }
        var camPosition = mainCam.transform.position;
        var position = useCameraOffset ? transform.position + cameraOffset : transform.position;
        var dist = Vector3.Distance(camPosition, position);
        foreach (var group in distanceGroups)
        {
            if (dist > group.minDistance && dist < group.maxDistance)
            {
                ActivateGroup(group);
            }
            else
            {
                DeactivateGroup(group);
            }
        }
    }

    void ActivateGroup(DistanceGroup group)
    {
        foreach (var go in group.GameObjects) go.SetActive(true);
    }

    void DeactivateGroup(DistanceGroup group)
    {
        foreach (var go in group.GameObjects) go.SetActive(false);
    }
}
