﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellBook : MonoBehaviour
{
    public static SpellBook Instance;

    public bool RewindEnabled => timeSpell.enabled;
    public bool MoveEnabled => moveSpell.enabled;
    public bool ImpulseEnabled => impulseSpell.enabled;

    TimeRewindModule timeSpell;
    PickUpModule moveSpell;
    ImpulseModule impulseSpell;
    TimeBodyManager manager;

    void Awake()
    {
        Instance = this;
        timeSpell = GetComponentInChildren<TimeRewindModule>();
        moveSpell = GetComponentInChildren<PickUpModule>();
        impulseSpell = GetComponentInChildren<ImpulseModule>();
        manager = TimeBodyManager.Instance;
    }

    public void Freeze(bool value)
    {
        timeSpell.enabled = !value;
        moveSpell.enabled = !value;
        impulseSpell.enabled = !value;
        if (value) ResetSpells();
    }

    public void ResetSpells()
    {
        moveSpell.SwitchState(PickUpModule.PickUpStates.Cooldown);
    }

    public void UnlockTimeSpell()
    {
        timeSpell.enabled = true;
    }

    public void UnlockMoveSpell()
    {
        moveSpell.enabled = true;
    }

    public void UnlockImpulseSpell()
    {
        impulseSpell.enabled = true;
    }
}
