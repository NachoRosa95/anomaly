﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Sirenix.OdinInspector;

[RequireComponent(typeof(TextMeshProUGUI))]
public class TypewriterEffect : MonoBehaviour
{
    private string savedText;
    private int curIndex = -1;
    private TextMeshProUGUI textComponent;
    private float t;
    private bool active = false;

    [SerializeField] float charactersPerSecond = 10;

    void OnEnable()
    {
        Execute();
    }

    void Update()
    {
        if(!active) return;
        var secondsPerCharacter = 1f / charactersPerSecond;
        t += Time.deltaTime;
        if(t > secondsPerCharacter)
        {
            t %= secondsPerCharacter;
            MoveNext();
        }
    }

    [Button("Execute")]
    public void Execute(string text = "")
    {
        if (!textComponent)
        {
            textComponent = GetComponent<TextMeshProUGUI>();
            savedText = textComponent.text;
        }
        if (text != "") savedText = text;
        curIndex = -1;
        // StopAllCoroutines();
        // StartCoroutine(TypeWriterEffect());
        textComponent.text = "";
        t = 0;
        active = true;
    }

    public bool MoveNext()
    {
        try
        {
            curIndex++;
            if (savedText[curIndex] == '<')
            {
                do
                {
                    curIndex++;
                } while (savedText[curIndex] != '>');
                curIndex++;
            }
            textComponent.text = savedText.Substring(0, curIndex + 1);
        }
        catch (System.Exception ex)
        {
            // Debug.LogError(ex.Message);
            active = false;
            return false;
        }
        return true;
    }

    IEnumerator TypeWriterEffect()
    {
        while (MoveNext())
        {
            yield return new WaitForSeconds(1f / charactersPerSecond);
        }
    }


}
