﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class UnityEvent_float_string : UnityEvent<float,string>
{

}

public class HealthComponent : MonoBehaviour
{
    [SerializeField] float maxHealth;
    [SerializeField] float health;
    [SerializeField] string memoryName = "health";

    public bool debug;

    private UnityEvent_float_string onChanged = new UnityEvent_float_string();

    public float MaxHealth
    {
        get { return maxHealth; }
        private set { maxHealth = value; }
    }

    public float Health
    {
        get { return health; }
        set
        {
            var lastHealth = health;
            health = Mathf.Clamp(value, 0, maxHealth);
            if (OnChanged != null) OnChanged.Invoke(health, memoryName);
            if (health == 0 && lastHealth > 0) SendMessage("OnDeath", SendMessageOptions.DontRequireReceiver);
        }
    }


    public UnityEvent_float_string OnChanged
    {
        get
        {
            return onChanged;
        }
    }

    private void Start()
    {
        Health = maxHealth;
    }

    public void Damage(float amount, GameObject damageDealer)
    {
        if(debug) Debug.Log("Damage");
        Health -= amount;
        SendMessage("OnDamage", damageDealer, SendMessageOptions.DontRequireReceiver);
    }

    public void Heal(float amount)
    {
        Health += amount;
    }
}
