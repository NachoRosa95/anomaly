﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class SelectGateElement : MonoBehaviour
{
    [SerializeField] List<GateElement> elements = new List<GateElement>();
    [SerializeField] bool startClosed;
    [SerializeField] bool selectFirst;
    [SerializeField] bool ignoreOpen;

    void Awake()
    {
        Init();
    }

    [Button("Init")]
    void Init()
    {
        elements.Clear();
        foreach (Transform child in transform)
        {
            var element = child.GetComponent<GateElement>();
            if (!element) continue;
            elements.Add(element);
            if(startClosed) element.ForceClose();
        }
        if (selectFirst) Execute(0);
    }

    public void Execute(int elementID)
    {
        for (var i = 0; i < elements.Count; i++)
        {
            var element = elements[i];
            if (!element) continue;
            if (elementID == i) element.Open();
            else if(!ignoreOpen) element.Close();
        }
    }

    public void Execute(Transform indexOrigin)
    {
        var elementID = indexOrigin.GetSiblingIndex();
        Execute(elementID);
    }

    public void ExecuteAmount(int amount)
    {
        for (var i = 0; i < elements.Count; i++)
        {
            var element = elements[i];
            if (!element) continue;
            if (i < amount) element.Open();
            else if(!ignoreOpen) element.Close();
        }
    }
}
