﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class PlayerManager : MonoBehaviour
{
    public GameObject Player;

    [SerializeField] Transform spawnsParent;
    [SerializeField] Transform secretsParent;

    private PlayerMovement playerMovement;
    private SpellBook spellBook;
    private List<Transform> spawns = new List<Transform>();
    private int curSpawnIndex = 0;

    public PlayerMovement PlayerMovement => playerMovement;
    private AnimatorController animatorController;
    private ReceptacleUser receptacleUser;

    void Awake()
    {
        animatorController = GetComponent<AnimatorController>();
        FetchPlayer();
        Init();
        spellBook = GetComponent<SpellBook>();

        var pivot = playerMovement.CameraController.CameraPivot;
        transform.position = pivot.position;
        transform.forward = pivot.forward;
    }

    void Update()
    {
        FetchPlayer();
        UpdateInput();
        var pivot = playerMovement.CameraController.CameraPivot;
        transform.position = pivot.position;
        transform.forward = pivot.forward;
        animatorController.Animator = playerMovement.BodyAnimator;
    }

    public void SetSpawnPoint(Transform reference)
    {
        playerMovement.SetSpawnPoint(reference);
    }

    [ButtonGroup("Spawn Controls")]
    [Button("Move to Previous Spawn")]
    public void MoveBack()
    {
        if (Application.isEditor) Init();
        curSpawnIndex--;
        curSpawnIndex = Mathf.Clamp(curSpawnIndex, 0, spawns.Count - 1);
        MoveTo(curSpawnIndex);
    }

    [ButtonGroup("Spawn Controls")]
    [Button("Move to Next Spawn")]
    public void MoveNext()
    {
        if (Application.isEditor) Init();
        curSpawnIndex++;
        curSpawnIndex = Mathf.Clamp(curSpawnIndex, 0, spawns.Count - 1);
        MoveTo(curSpawnIndex);
    }

    public void MoveTo(int spawnIndex)
    {
        try { MoveTo(spawns[spawnIndex]); }
        catch (System.Exception ex)
        {
            Debug.LogError(ex.Message, this);
        }
    }

    public void MoveTo(Transform reference)
    {
        Player.transform.position = reference.position;
    }

    public void UnlockTimeSpell(int level)
    {
        spellBook.UnlockTimeSpell();
    }

    public void UnlockMoveSpell(int level)
    {
        spellBook.UnlockMoveSpell();
    }

    public void UnlockImpulseSpell(int level)
    {
        spellBook.UnlockImpulseSpell();
    }

    public void ResetSpells()
    {
        spellBook.ResetSpells();
    }

    public void FreezePlayer(bool value)
    {
        Debug.Log("Freezing player " + value, this);
        if (!playerMovement) FetchPlayer();
        if (!playerMovement) return;
        playerMovement.Freeze(value);
        spellBook.Freeze(value);
    }

    public void HidePlayer(bool value)
    {
        Debug.Log("Hiding player " + value, this);
        if (!playerMovement) FetchPlayer();
        if (!playerMovement) return;
        playerMovement.Hide(value);
    }

    void FetchPlayer()
    {
        if (Player == null)
        {
            Player = GameObject.FindWithTag("Player");
        }
        if (playerMovement == null) playerMovement = Player.GetComponent<PlayerMovement>();
        if (receptacleUser == null) receptacleUser = Player.GetComponent<ReceptacleUser>();
    }

    void Init()
    {
        if (spawns.Count > 0) return;
        foreach (Transform spawn in spawnsParent)
        {
            spawns.Add(spawn);
        }
    }

    void UpdateInput()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            MoveBack();
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            MoveNext();
        }
        if (Input.GetKeyDown(KeyCode.Period))
        {
            if (Time.timeScale < 10) Time.timeScale += 1;
        }
        if (Input.GetKeyDown(KeyCode.Comma))
        {
            if (Time.timeScale > 1) Time.timeScale -= 1;
        }
        if (Time.timeScale > 0 && Input.GetKeyDown(KeyCode.F))
        {
            receptacleUser.Activate();
        }
        else
        {
            if (receptacleUser.Receptacles.Count == 0)
            {
                foreach (Transform secret in secretsParent)
                {
                    var gate = secret.GetComponent<GateElement>();
                    if (gate) gate.Close();
                    else secret.gameObject.SetActive(false);
                }
            }
        }
    }

}
