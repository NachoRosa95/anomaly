﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorController : MonoBehaviour
{
    [SerializeField] Animator animator;

    public Animator Animator
    {
        get => animator;
        set => animator = value;
    }

    public void SetTrigger(string name)
    {
        animator.SetTrigger(name);
    }

    public void SetBoolTrue(string name)
    {
        SetBool(name, true);
    }

    public void SetBoolFalse(string name)
    {
        SetBool(name, false);
    }

    public void SetBool(string name, bool value)
    {
        animator.SetBool(name, value);
    }

    public void SetFloat(string name, float value)
    {
        animator.SetFloat(name, value);
    }

    public void SetInt(string name, int value)
    {
        animator.SetInteger(name, value);
    }
}
