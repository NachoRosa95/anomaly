﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class TimeRewindModule : MonoBehaviour
{
    [SerializeField] LayerMask layerMask;
    [SerializeField] AnimationCurve audioLoopVolume;
    [SerializeField] float range = 2;
    [SerializeField] float cooldown = 0.2f;
    [SerializeField] float rewindSpeed = 2;

    [FoldoutGroup("Audio")]
    [SerializeField] AudioSource audioOn;
    [FoldoutGroup("Audio")]
    [SerializeField] AudioSource audioOff;
    [FoldoutGroup("Audio")]
    [SerializeField] AudioSource audioLoopBack;
    [FoldoutGroup("Audio")]
    [SerializeField] AudioSource audioLoopForward;

    private GameObject highlightedItem;
    private List<GameObject> selectedItems = new List<GameObject>();

    private TimeController lastInteracted;
    private PickUpModule pickUpModule;
    private Animator cameraAnimator;
    private PlayerManager playerManager;
    private bool isKinematic;
    private bool useGravity;
    private int layer;
    private float timeLastRewind;
    private RewindState rewindState;
    private bool wasRewinding;

    private enum RewindState
    {
        None,
        Back,
        Forward
    }

    public List<GameObject> SelectedItems
    {
        get => selectedItems;
        set => selectedItems = value;
    }

    void Awake()
    {
        pickUpModule = GetComponent<PickUpModule>();
        cameraAnimator = Camera.main.GetComponent<Animator>();
        playerManager = GetComponent<PlayerManager>();
    }

    void Update()
    {
        CastRay();
        HandleInput();
        PlayLoop();
    }

    public bool Contains(GameObject gameObject)
    {
        return selectedItems.Contains(gameObject);
    }

    void PlayLoop()
    {
        var timeSinceLastRewind = Time.time - timeLastRewind;

        if (rewindState == RewindState.Forward)
        {
            audioLoopForward.volume = audioLoopVolume.Evaluate(timeSinceLastRewind / audioLoopForward.clip.length);
            audioLoopBack.volume = Mathf.MoveTowards(audioLoopBack.volume, 0, Time.deltaTime);
        }
        else if (rewindState == RewindState.Back)
        {
            audioLoopBack.volume = audioLoopVolume.Evaluate(timeSinceLastRewind / audioLoopBack.clip.length);
            audioLoopForward.volume = Mathf.MoveTowards(audioLoopForward.volume, 0, Time.deltaTime);
        }
        else
        {
            audioLoopBack.volume = Mathf.MoveTowards(audioLoopBack.volume, 0, Time.deltaTime);
            audioLoopForward.volume = Mathf.MoveTowards(audioLoopForward.volume, 0, Time.deltaTime);
        }
    }

    void CastRay()
    {
        var ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, range, layerMask))
        {
            // Debug.Log(hit.transform.gameObject);
            var timeController = hit.transform.GetComponentInParent<TimeController>();
            if (timeController && timeController.enabled)
            {
                ClearHighlight();
                highlightedItem = timeController.transform.gameObject;
                lastInteracted = timeController;
                timeController.Highlighted = true;
            }
        }
        else
        {
            ClearHighlight();
        }
    }

    void ClearHighlight()
    {
        highlightedItem = null;
        if (lastInteracted && !SelectedItems.Contains(lastInteracted.gameObject)) lastInteracted.Highlighted = false;
    }

    void AddToSelection(GameObject go)
    {
        if (!selectedItems.Contains(go))
        {
            if (pickUpModule.Selected == go) pickUpModule.SwitchState(PickUpModule.PickUpStates.Cooldown);

            var timeController = go.GetComponent<TimeController>();
            if (timeController.Paused)
            {
                if (audioOff) audioOff.Play();
                timeController.Paused = false;
            }
            else
            {
                selectedItems.Add(go);
                PauseItem(go, true);
            }
        }
        else
        {
            selectedItems.Remove(go);
            PauseItem(go, false);
        }
        HighlightSelected(true);
    }

    void ClearSelection()
    {
        HighlightSelected(false);
        PauseSelected(false);
        selectedItems.Clear();
    }

    void HighlightSelected(bool value)
    {
        foreach (GameObject item in selectedItems)
        {
            var timeController = item.GetComponent<TimeController>();
            if (timeController) timeController.Highlighted = value;
        }
    }

    void HandleInput()
    {
        if (highlightedItem && Input.GetButtonDown("Freeze"))
        {
            AddToSelection(highlightedItem);
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            ClearSelection();
        }

        var rewind = Input.GetAxis("Rewind") * Time.deltaTime * rewindSpeed;
        RewindSelected(rewind);
    }

    void PauseSelected(bool value)
    {
        foreach (GameObject item in selectedItems)
        {
            PauseItem(item, value);
        }
    }

    public void PauseItem(GameObject item, bool value)
    {

        var timeController = item.GetComponent<TimeController>();
        if (!timeController) return;

        if (value && audioOn) audioOn.Play();
        if (!value && audioOff) audioOff.Play();
        timeController.Paused = value;
        var handRightAnimator = playerManager.PlayerMovement.HandRightAnimator;
        if (handRightAnimator)
        {
            if (value) handRightAnimator.SetTrigger("TimeStop");
            if (!value) handRightAnimator.SetTrigger("TimeResume");
        }
    }

    void RewindSelected(float value)
    {
        if (!cameraAnimator) cameraAnimator = Camera.main.GetComponent<Animator>();
        var handRightAnimator = playerManager.PlayerMovement.HandRightAnimator;

        if (selectedItems != null && selectedItems.Count > 0)
        {
            rewindState = Mathf.Approximately(value, 0f) ? RewindState.None : value > 0 ? RewindState.Forward : RewindState.Back;
            var isRewinding = rewindState != RewindState.None;

            if (isRewinding && !wasRewinding)
            {
                if (value >= 0)
                    audioLoopForward.Play();
                if (value <= 0)
                    audioLoopBack.Play();
                timeLastRewind = Time.time;
            }

            handRightAnimator.SetBool("RewindForward", isRewinding);
            if (value >= 0)
            {
                cameraAnimator.SetBool("Forwarding", rewindState == RewindState.Forward);
                // handRightAnimator.SetBool("RewindBack", rewindState == RewindState.Back);
            }
            if (value <= 0)
            {
                cameraAnimator.SetBool("Rewinding", isRewinding);
                // handRightAnimator.SetBool("RewindForward", isRewinding);
            }

            wasRewinding = isRewinding;
        }
        else
        {
            cameraAnimator.SetBool("Rewinding", false);
            cameraAnimator.SetBool("Forwarding", false);
            handRightAnimator.SetBool("RewindBack", false);
            handRightAnimator.SetBool("RewindForward", false);
        }

        foreach (GameObject item in selectedItems)
        {
            var timeController = item.GetComponent<TimeController>();
            if (!timeController) continue;
            timeController.Paused = true;
            timeController.RewindTime -= value;
        }
    }

}
