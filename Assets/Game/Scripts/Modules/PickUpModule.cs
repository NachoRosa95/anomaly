﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpModule : MonoBehaviour
{
    [SerializeField] LayerMask layerMask;
    [SerializeField] Image crosshairs;
    [SerializeField] int selectedLayer;
    [SerializeField] float moveSpeedMultiplier = 10;
    [SerializeField] float maxMoveSpeed = 10;
    [SerializeField] float range = 2;
    [SerializeField] float pickUpOffset = 0.5f;
    [SerializeField] float cooldown = 0.2f;

    private GameObject selected;
    private Haulable lastInteracted;
    private PickUpStates state = PickUpStates.Selecting;
    private TimeRewindModule timeRewind;
    private PlayerManager playerManager;
    private bool isKinematic;
    private bool useGravity;
    private int initLayer;

    public GameObject Selected => selected;

    public enum PickUpStates
    {
        Selecting,
        Moving,
        Cooldown
    }

    void Awake()
    {
        timeRewind = GetComponent<TimeRewindModule>();
        playerManager = GetComponent<PlayerManager>();
    }

    void OnEnable()
    {
        SwitchState(PickUpStates.Selecting);
        crosshairs.gameObject.SetActive(true);
    }

    void OnDisable()
    {
        StopMoving();
        crosshairs.gameObject.SetActive(false);
    }

    void FixedUpdate()
    {
        UpdateState();
        HandleInput();
        if(crosshairs) crosshairs.color = Selected ? Color.cyan : Color.white;
    }

    void UpdateState()
    {
        switch (state)
        {
            case PickUpStates.Selecting:
                CastRay();
                break;
            case PickUpStates.Moving:
                Move();
                break;
            default:
                break;
        }
    }

    void CastRay()
    {
        var ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, range, layerMask))
        {
            var interactible = hit.transform.GetComponentInParent<Haulable>();
            if (interactible && interactible.enabled)
            {
                ClearSelection();
                selected = interactible.transform.gameObject;
                lastInteracted = interactible;
                interactible.Highlighted = true;
            }
        }
        else
        {
            ClearSelection();
        }
    }

    void ClearSelection()
    {
        selected = null;
        if (lastInteracted)
        {
            lastInteracted.Highlighted = false;
            lastInteracted.Hauling = false;
        }
    }

    void HandleInput()
    {
        switch (state)
        {
            case PickUpStates.Selecting:
                if (selected && Input.GetButton("Interact") && !playerManager.PlayerMovement.JumpedRecently)
                {
                    SwitchState(PickUpStates.Moving);
                    lastInteracted.Hauling = true;
                }
                break;
            case PickUpStates.Moving:
                if (selected && (!Input.GetButton("Interact") || Input.GetKeyUp(KeyCode.F)))
                {
                    SwitchState(PickUpStates.Cooldown);
                    lastInteracted.Hauling = false;
                }
                break;
            default:
                break;
        }
    }

    public void SwitchState(PickUpStates newState)
    {
        if (newState == state) return;
        EndState(state);
        StartState(newState);
        state = newState;
    }

    void StartState(PickUpStates newState)
    {
        switch (newState)
        {
            case PickUpStates.Selecting:
                break;
            case PickUpStates.Moving:
                StartMoving();
                break;
            case PickUpStates.Cooldown:
                StartCooldown();
                break;
            default:
                break;
        }
    }

    void StartCooldown()
    {
        Invoke("StopCooldown", cooldown);
    }

    void StopCooldown()
    {
        SwitchState(PickUpStates.Selecting);
    }

    void EndState(PickUpStates newState)
    {
        switch (newState)
        {
            case PickUpStates.Selecting:
                break;
            case PickUpStates.Moving:
                StopMoving();
                break;
            default:
                break;
        }
    }

    void StartMoving()
    {
        if (!selected) { Debug.LogError("No selection", this); return; }

        var handLeftAnimator = playerManager.PlayerMovement.HandLeftAnimator;
        if (handLeftAnimator) handLeftAnimator.SetBool("Grab", true);
        var rb = selected.GetComponent<Rigidbody>();

        if (timeRewind.Contains(selected))
        {
            timeRewind.PauseItem(selected, false);
            timeRewind.SelectedItems.Remove(selected);
        }

        useGravity = rb.useGravity;
        isKinematic = rb.isKinematic;
        initLayer = selected.layer;

        rb.useGravity = false;
        rb.isKinematic = false;
        selected.gameObject.layer = selectedLayer;
    }

    void Move()
    {
        if (!selected)
        {
            SwitchState(PickUpStates.Cooldown);
            Debug.LogError("No selection", this); return;
        }

        var rb = selected.GetComponent<Rigidbody>();
        var desiredDistance = selected.transform.localScale.magnitude / 2 + pickUpOffset;
        var desiredPosition = transform.position + transform.forward * desiredDistance;
        var diff = desiredPosition - selected.transform.position;
        var velocity = diff * moveSpeedMultiplier * Time.fixedDeltaTime;
        if (diff.magnitude > 0.001f)
            rb.AddForce(velocity * Time.deltaTime, ForceMode.Impulse);
        else
            rb.velocity = Vector3.zero;
        if (rb.velocity.magnitude > maxMoveSpeed) rb.velocity = velocity.normalized * maxMoveSpeed;
        rb.angularVelocity = Vector3.zero;
    }

    void StopMoving()
    {
        if (!selected)
        {
            return;
        }

        var handLeftAnimator = playerManager.PlayerMovement.HandLeftAnimator;
        if (handLeftAnimator) handLeftAnimator.SetBool("Grab", false);
        var rb = selected.GetComponent<Rigidbody>();

        if (!lastInteracted.Throwable) rb.velocity = Vector3.zero;
        rb.useGravity = useGravity;
        rb.isKinematic = isKinematic;
        selected.gameObject.layer = initLayer;

        ClearSelection();
    }
}

