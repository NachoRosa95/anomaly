﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpulseModule : MonoBehaviour
{
    [SerializeField] LayerMask layerMask;
    [SerializeField] float range = 2;
    [SerializeField] float magnitude = 10;

    private Impulsable highlightedItem;
    private float charge = 0;

    const float chargeRate = 1;

    void Update()
    {
        CastRay();
        HandleInput();
    }

    void CastRay()
    {
        var ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, range, layerMask))
        {
            var impulsable = hit.transform.GetComponent<Impulsable>();
            if (impulsable)
            {
                ClearHighlight();
                highlightedItem = impulsable;
                highlightedItem.Highlighted = true;
            }
        }
        else
        {
            ClearHighlight();
        }
    }

    void ClearHighlight()
    {
        if (highlightedItem == null || charge > 0) return;
        highlightedItem.Highlighted = false;
        Release();
        highlightedItem = null;
    }

    void HandleInput()
    {
        if (highlightedItem && Input.GetButton("Impulse"))
        {
            if (charge < 1)
            {
                charge += chargeRate * Time.deltaTime;
                AddForce();
            }
        }
    }

    void LateUpdate()
    {
        if (Input.GetKeyUp(KeyCode.F))
        {
            Release();
        }
    }

    void Release()
    {
        if (highlightedItem) highlightedItem.ApplyForce();
        charge = 0;
    }

    void AddForce()
    {
        if (highlightedItem == null) return;

        // Vector3 diff = HighlightedItem.transform.position - transform.position;
        // Vector3 force = diff.normalized * magnitude;
        // Vector3 force = transform.forward * magnitude * charge;

        Vector3 force = transform.forward * magnitude * chargeRate * Time.deltaTime;
        highlightedItem.AddForce(force);
        Debug.Log("Adding force " + force);
    }
}
