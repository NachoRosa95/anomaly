﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[ExecuteInEditMode]
public class PostProcessModifier : MonoBehaviour
{
    private PostProcessVolume volume;

    [SerializeField] float saturation;
    [SerializeField] float grainIntensity;
    [SerializeField] float vignetteIntensity;
    [SerializeField] float temperature;

    ColorGrading colorGrading = null;
    Grain grain = null;
    Vignette vignette = null;

    float initSaturation;

    void Awake()
    {
        volume = GetComponent<PostProcessVolume>();
        volume.sharedProfile.TryGetSettings(out colorGrading);
        volume.sharedProfile.TryGetSettings(out grain);
        volume.sharedProfile.TryGetSettings(out vignette);
    }

    void Update()
    {
        colorGrading.saturation.value = saturation;
        colorGrading.temperature.value = temperature;
        grain.intensity.value = grainIntensity;
        vignette.intensity.value = vignetteIntensity;
    }
}
