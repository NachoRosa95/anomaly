﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class LaserBeam : MonoBehaviour
{
    [SerializeField] List<LineRenderer> lines = new List<LineRenderer>();
    [SerializeField] Transform origin;
    [SerializeField] Transform end;

    void Update()
    {
        UpdateLines();
    }

    void UpdateLines()
    {
        if (!origin || !end) return;

        foreach (var line in lines)
        {
            if (!line) continue;
            if (line.positionCount != 2) line.positionCount = 2;
            line.SetPosition(0, origin.position);
            line.SetPosition(1, end.position);
        }
    }
}
