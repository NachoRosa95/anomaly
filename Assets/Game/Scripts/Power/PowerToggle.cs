﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class BoolEvent : UnityEvent<bool>
{

}

public class PowerToggle : PowerSource
{
    public BoolEvent OnToggleStateChanged = new BoolEvent();

    public void Toggle()
    {
        Activated = !Activated;
    }

    protected override void Awake()
    {
        base.Awake();
        OnActivated.AddListener(ToggleStateChanged);
        OnDeactivated.AddListener(ToggleStateChanged);
    }
    private void ToggleStateChanged()
    {
        OnToggleStateChanged.Invoke(Activated);
    }
}
