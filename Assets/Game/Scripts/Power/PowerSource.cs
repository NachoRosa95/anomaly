﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

public abstract class PowerSource : MonoBehaviour
{
    [SerializeField] [HideInInspector] protected bool activated;

    [FoldoutGroup("PowerSource", true)]
    public UnityEvent OnActivated = new UnityEvent();

    [FoldoutGroup("PowerSource", true)]
    public UnityEvent OnDeactivated = new UnityEvent();

    // For gizmos
    List<Transform> activatedOutputs = new List<Transform>();
    List<Transform> deactivatedOutputs = new List<Transform>();

    [ShowInInspector]
    [FoldoutGroup("PowerSource", true)]
    [PropertyOrder(-1)]
    public bool Activated
    {
        get => activated;
        set
        {
            if (activated == value) return;
            activated = value;
            if (Application.isPlaying)
            {
                if (activated) OnActivated?.Invoke();
                if (!activated) OnDeactivated?.Invoke();
            }
        }
    }


    protected virtual void Awake()
    {
        if (activated) OnActivated.Invoke();
        else OnDeactivated.Invoke();
    }

    void OnDrawGizmos()
    {
        // Ugly way to update very rarely on low priority operations
        var random = Random.value;
        if (random < 0.005f)
        {
            activatedOutputs = GetEventOutputs(OnActivated);
            deactivatedOutputs = GetEventOutputs(OnDeactivated);
        }

        Gizmos.color = Color.green;
        foreach (Transform output in activatedOutputs)
        {
            Gizmos.DrawLine(transform.position, output.transform.position);
        }

        Gizmos.color = Color.red;
        foreach (Transform output in deactivatedOutputs)
        {
            Gizmos.DrawLine(transform.position, output.transform.position);
        }
    }

    List<Transform> GetEventOutputs(UnityEvent targetEvent)
    {
        var outputs = new List<Transform>();
        var eventCount = targetEvent.GetPersistentEventCount();
        for (var i = 0; i < eventCount; i++)
        {
            var obj = targetEvent.GetPersistentTarget(i);
            if (obj == null) continue;
            var comp = obj as Component;
            if (comp) outputs.Add(comp.transform);
            else
            {
                var go = obj as GameObject;
                if (go) outputs.Add(go.transform);
            }

        }
        return outputs;
    }
}
