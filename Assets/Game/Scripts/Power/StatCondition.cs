﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatCondition : PowerSource
{
    [SerializeField] string statType;
    [SerializeField] ComparatorOp comparatorOp;
    [SerializeField] int countRequired;

    List<string> statRegister;

    void Awake()
    {
        statRegister = StatsManager.Instance.GetRegister(statType);
    }

    void Update()
    {
        if (comparatorOp == ComparatorOp.Equals) Activated = statRegister.Count == countRequired;
        if (comparatorOp == ComparatorOp.GreaterThan) Activated = statRegister.Count >= countRequired;
        if (comparatorOp == ComparatorOp.LessThan) Activated = statRegister.Count <= countRequired;
    }
}
