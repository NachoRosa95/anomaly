﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReceptacleUser : MonoBehaviour
{
    public List<Receptacle> Receptacles = new List<Receptacle>();

    public void Activate()
    {
        for (var i = 0; i < Receptacles.Count; i++)
        {
            var receptacle = Receptacles[i];
            if (!receptacle) continue;
            var animEvents = receptacle.GetComponent<AnimEvents>();
            animEvents.InvokeEvent(0);
            if (!Receptacles.Contains(receptacle)) i--;
        }
    }
}
