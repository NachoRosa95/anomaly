﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

[System.Serializable]
public class IntEvent : UnityEvent<int>
{

}

public enum ComparatorOp
{
    Equals,
    GreaterThan,
    LessThan
}

public class Receptacle : PowerSource
{
    [FoldoutGroup("Setup", true)]
    public IntEvent OnInputChanged = new IntEvent();

    [SerializeField] List<ReceptacleUser> users;

    [FoldoutGroup("Setup", true)]
    [SerializeField] Transform indicatorSlotsParent;

    [FoldoutGroup("Setup", true)]
    [SerializeField] Animator indicatorPrefab;

    [FoldoutGroup("Setup", true)]
    [SerializeField] ComparatorOp Op = ComparatorOp.Equals;

    [FoldoutGroup("Setup", true)]
    [SerializeField] int countRequired;

    [FoldoutGroup("Setup", true)]
    [SerializeField] bool debug;

    List<Animator> indicators = new List<Animator>();
    AnimEvents animEvents;

    void Awake()
    {
        base.Awake();

        if (indicatorSlotsParent && indicatorPrefab)
        {
            for (var i = 0; i < countRequired; i++)
            {
                var slot = indicatorSlotsParent.GetChild(i);
                if (!slot) break;
                if (slot.childCount > 0) continue;
                var newIndicator = Instantiate(indicatorPrefab, slot.position, slot.rotation, slot);
                indicators.Add(newIndicator);
            }
        }

        UsersChanged(users.Count);

        animEvents = GetComponent<AnimEvents>();
    }

    void OnDisable()
    {
        var limit = 1000;
        var i = 0;
        while (users.Count > 0)
        {
            if (++i > limit)
            {
                Debug.LogError("Possible infinite loop prevented", this);
                return;
            }
            var user = users[0];
            RemoveUser(user);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        var user = other.GetComponentInParent<ReceptacleUser>();
        AddUser(user);
    }

    private void OnTriggerExit(Collider other)
    {
        var user = other.GetComponentInParent<ReceptacleUser>();
        RemoveUser(user);
    }

    private void AddUser(ReceptacleUser user)
    {
        if (!user || users.Contains(user)) return;
        var oldCount = users.Count;
        users.Add(user);
        user.Receptacles.Add(this);
        UsersChanged(oldCount);
    }

    private void RemoveUser(ReceptacleUser user)
    {
        if (!user || !users.Contains(user)) return;
        var oldCount = users.Count;
        users.Remove(user);
        user.Receptacles.Remove(this);
        UsersChanged(oldCount);
    }

    private void UsersChanged(int oldCount)
    {
        if (debug) Debug.Log($"UsersChanged {countRequired} {users.Count}");
        OnInputChanged?.Invoke(users.Count);

        if (Op == ComparatorOp.Equals) Activated = users.Count == countRequired;
        if (Op == ComparatorOp.GreaterThan) Activated = users.Count > countRequired;
        if (Op == ComparatorOp.LessThan) Activated = users.Count < countRequired;

        var i = 0;
        foreach (var indicator in indicators)
        {
            if (i++ < users.Count)
            {

                if (debug) Debug.Log("Open");
                indicator.SetBool("Open", true);
            }
            else
            {

                if (debug) Debug.Log("Close");
                indicator.SetBool("Open", false);
            }
        }
    }
}
