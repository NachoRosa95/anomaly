﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PowerGate : PowerSource
{
    public enum GateMode
    {
        And,
        Or,
        Not
    }

    public GateMode Mode;
    public List<PowerSource> Inputs = new List<PowerSource>();

    void Awake()
    {
        
    }

    void Update()
    {
        UpdateGate();
    }

    void UpdateGate()
    {
        switch(Mode)
        {
            case GateMode.And:
                Activated = Inputs.All(source => source.Activated);
            break;

            case GateMode.Or:
                Activated = Inputs.Any(source => source.Activated);
            break;

            case GateMode.Not:
                if(Inputs.Count > 1) Debug.LogError("PowerGate in Not mode has multiple inputs, only the first one will be used", this);
                if(Inputs.Count > 0) Activated = !Inputs[0].Activated;
            break;
            
            default:
            break;
        }
    }
}
