﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pad : Button
{
    private enum PadMode
    {
        Toggle,
        Hold,
    }

    [SerializeField] PadMode padMode = PadMode.Toggle;
    private bool toggleDisabled = false;

    protected override void Awake()
    {
        base.Awake();
    }

    void FixedUpdate()
    {
        SetMaterial(Activated);
    }

    void CollisionStayed()
    {
        ActivatePad();
    }

    void CollisionExited()
    {
        DeactivatePad();
    }

    public void ActivatePad()
    {
        if (padMode == PadMode.Toggle && !toggleDisabled)
        {
            Debug.Log(Activated + " " + toggleDisabled);
            toggleDisabled = true;
            Activated = !Activated;
        }
        else if (padMode == PadMode.Hold)
        {
            Activated = !reverse;
        }
    }

    public void DeactivatePad()
    {
        if (padMode == PadMode.Toggle)
        {
            toggleDisabled = false;
        }
        else
        {
            Activated = !Activated;
        }
    }
}
