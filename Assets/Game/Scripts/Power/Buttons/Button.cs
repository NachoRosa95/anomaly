﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class Button : PowerSource
{
    private MeshRenderer buttonMesh;

    [SerializeField] protected Transform button;
    [SerializeField] protected Material onMaterial;
    [SerializeField] protected Material offMaterial;
    [SerializeField] protected bool reverse;

    protected virtual void Awake()
    {
        if (!button) button = transform.Find("Button");
        if (!button) Debug.LogError("No button in children", this);
        buttonMesh = button.GetComponentInChildren<MeshRenderer>();
        if (!buttonMesh) Debug.LogWarning("No meshRenderer attached to button", this);
        if (!offMaterial) offMaterial = buttonMesh.material;
        if (reverse) Activated = true;
        SetMaterial(Activated);
    }

    protected void SetMaterial(bool on)
    {
        buttonMesh.material = on ? onMaterial ?? offMaterial : offMaterial;
    }
}
