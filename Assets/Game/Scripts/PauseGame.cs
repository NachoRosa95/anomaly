﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class PauseGame : MonoBehaviour
{
    [SerializeField] GameObject instructions;
    [SerializeField] float resetTime = 5;

    bool paused = false;

    public UnityEvent OnPause = new UnityEvent();
    public UnityEvent OnUnpause = new UnityEvent();

    void Awake()
    {
        UpdatePause();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            if (instructions) instructions.SetActive(!instructions.activeSelf);
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            paused = !paused;
            UpdatePause();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            StartCoroutine(ResetCoroutine());
        }
        if (!Input.GetKey(KeyCode.R))
        {
            StopAllCoroutines();
        }
    }

    void UpdatePause()
    {
        if (paused)
        {
            Pause(false);
            OnPause?.Invoke();
        }
        if (!paused)
        {
            Pause(true);
            OnUnpause?.Invoke();
        }
    }

    public void Pause(bool off)
    {
        Time.timeScale = off ? 1 : 0;
    }

    void Reset()
    {
        var thisScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(thisScene.name);
    }

    IEnumerator ResetCoroutine()
    {
        yield return new WaitForSeconds(resetTime);
        Reset();
    }
}
