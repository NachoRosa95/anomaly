﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
[ExecuteInEditMode]
public class SlidingJoint : MonoBehaviour
{
    [SerializeField, Tooltip("The transform that acts one end of the joint")]
    Transform connectedTransform;

    [SerializeField, Tooltip("How much mass is required to counteract the joint's force, use 0 for a joint with no resistance")]
    float counterWeight = 1;

    [SerializeField, Tooltip("If true the attached rigidBody can move freely between its current position and connectedTransform's position")]
    bool autoConfigure;

    [SerializeField, Tooltip("How far from connectedTransform is the rigidbody allowed to move")]
    float maxDistance;

    [SerializeField, Tooltip("How close to connectedTransform is the rigidbody allowed to move")]
    float minDistance;

    [SerializeField, Tooltip("How close to max/min distance does the rigidbody have to be to activate Enter and Exit events")]
    float activateDistance = 0.01f;

    [SerializeField] bool motorDriven;
    [SerializeField, Range(0, 1f)] float motorStep;

    public bool affectScale;
    public UnityEvent OnMinDistanceEnter;
    public UnityEvent OnMinDistanceExit;
    public UnityEvent OnMaxDistanceEnter;
    public UnityEvent OnMaxDistanceExit;

    private Rigidbody rb;
    private Vector3 diff;
    private float step;
    private float initScale;
    private float rangeOfMotion;
    private bool configured;

    private JointState jointState;
    private enum JointState
    {
        MaxDistance,
        MinDistance,
        Moving
    }

    void OnGUI()
    {
        if (autoConfigure)
        {
            maxDistance = Vector3.Distance(transform.position, connectedTransform.position);
            minDistance = 0;
        }
        if (minDistance > maxDistance) minDistance = maxDistance;
        activateDistance = Mathf.Clamp(activateDistance, 0, (maxDistance - minDistance)/2f);
    }

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        rangeOfMotion = maxDistance - minDistance;
        if (rangeOfMotion == 0)
        {
            Debug.LogError("Range of motion is 0", this);
            enabled = false;
        }
        initScale = transform.localScale.y;
    }

    void FixedUpdate()
    {
        maxDistance = Mathf.Clamp(maxDistance, minDistance, float.MaxValue);
        minDistance = Mathf.Clamp(minDistance, 0, maxDistance);

        if (counterWeight == 0 || motorDriven) return;
        var magnitude = counterWeight * -Physics.gravity.y;
        var force = transform.up * magnitude;
        rb.AddForce(force, ForceMode.Force);
    }

    void LateUpdate()
    {

        if (motorDriven)
        {
            var offset = minDistance + motorStep * rangeOfMotion;
            transform.position = (connectedTransform.position + transform.up * offset);
        }

        diff = transform.position - connectedTransform.position;
        diff = transform.TransformDirection(diff);
        if (diff.y >= maxDistance - activateDistance)
        {
            SwitchState(JointState.MaxDistance);
            if (diff.y >= maxDistance)
            {
                transform.position = (connectedTransform.position + transform.up * maxDistance);
                rb.velocity = Vector3.zero;
            }
        }
        else if (diff.y <= minDistance + activateDistance)
        {
            SwitchState(JointState.MinDistance);
            if (diff.y <= minDistance)
            {
                transform.position = (connectedTransform.position + transform.up * minDistance);
                rb.velocity = Vector3.zero;
            }
        }
        else
        {
            SwitchState(JointState.Moving);
        }

        diff = transform.position - connectedTransform.position;
        step = diff.magnitude / rangeOfMotion;
        if (affectScale)
        {
            var newScale = transform.localScale;
            newScale.y = step * initScale;
            transform.localScale = newScale;
        }
    }

    // Overkill but ok
    void SwitchState(JointState state)
    {
        if (state == jointState) return;

        // Exit state
        switch (jointState)
        {
            case JointState.MaxDistance:
                if (OnMaxDistanceExit != null) OnMaxDistanceExit.Invoke();
                break;
            case JointState.MinDistance:
                if (OnMinDistanceExit != null) OnMinDistanceExit.Invoke();
                break;
            case JointState.Moving:
                break;
        }

        // Enter state
        switch (state)
        {
            case JointState.MaxDistance:
                if (OnMaxDistanceExit != null) OnMaxDistanceEnter.Invoke();
                break;
            case JointState.MinDistance:
                if (OnMinDistanceEnter != null) OnMinDistanceEnter.Invoke();
                break;
            case JointState.Moving:
                break;
        }

        jointState = state;
    }

    void OnDrawGizmos()
    {
        if (!connectedTransform) return;
        if (autoConfigure && !configured)
        {
            configured = true;
            maxDistance = Vector3.Distance(transform.position, connectedTransform.position);
            minDistance = 0;
        }
        Gizmos.color = Color.magenta;
        var a = connectedTransform.position + transform.up * minDistance;
        var b = connectedTransform.position + transform.up * maxDistance;
        var c = a + transform.up * activateDistance;
        var d = b - transform.up * activateDistance;
        Gizmos.DrawLine(a, c);
        Gizmos.DrawLine(d, b);
        Gizmos.DrawCube(a, Vector3.one * 0.005f);
        Gizmos.DrawCube(b, Vector3.one * 0.005f);
        Gizmos.color = Color.red;
        Gizmos.DrawLine(c, d);
        Gizmos.color = Color.blue;
        Gizmos.DrawCube(transform.position, Vector3.one * 0.005f);
    }
}
