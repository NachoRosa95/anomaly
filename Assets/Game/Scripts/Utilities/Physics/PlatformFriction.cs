﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class PlatformFriction : MonoBehaviour
{
    [SerializeField] PhysicMaterial onPlatformMaterial;
    [SerializeField] PhysicMaterial offPlatformMaterial;
    Collider col;
    Transform platform;

    void Awake()
    {
        col = GetComponent<Collider>();
    }

    void Update()
    {
        if (!IsOnPlatform()) SetMaterial(null);
    }

    void OnCollisionStay(Collision collision)
    {
        if (!enabled) return;
        if (collision.gameObject.tag != "Platform") return;
        foreach (ContactPoint point in collision.contacts)
        {
            if (point.normal.y < 0.5) continue;
            SetMaterial(collision.transform);
            platform = collision.transform;
        }
    }

    void SetMaterial(Transform transform)
    {
        platform = transform;
        col.material = transform ? onPlatformMaterial : offPlatformMaterial;
    }

    bool IsOnPlatform()
    {
        if (!platform) return false;
        var otherCollider = platform.GetComponentInChildren<Collider>();
        if (Vector3.Distance(transform.position, otherCollider.ClosestPoint(transform.position)) > 0.1f) return false;
        return true;
    }
}
