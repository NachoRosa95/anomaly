﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollisionEvents : MonoBehaviour
{
    public UnityEvent TriggerEnter = new UnityEvent();
    public UnityEvent TriggerStay = new UnityEvent();
    public UnityEvent TriggerExit = new UnityEvent();

    void Awake()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        TriggerEnter?.Invoke();
    }

    void OnTriggerStay(Collider other)
    {
        TriggerStay?.Invoke();
    }

    void OnTriggerExit(Collider other)
    {
        TriggerExit?.Invoke();
    }
}
