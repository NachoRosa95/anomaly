﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimEvents : MonoBehaviour
{
    public List<UnityEvent> Events = new List<UnityEvent>();

    [SerializeField] bool debug;

    public void InvokeEvent(int index)
    {
        if (debug) Debug.Log("Invoking anim event " + index, this);
        if (index >= 0 && Events.Count > index)
        {
            Events[index]?.Invoke();
        }
    }

    public void InvokeAllEvents()
    {
        for (var i = 0; i < Events.Count; i++) InvokeEvent(i);
    }
}
