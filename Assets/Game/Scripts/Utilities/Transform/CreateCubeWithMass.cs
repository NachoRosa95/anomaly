﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateCubeWithMass : MonoBehaviour
{
    [SerializeField] float mass;
    [SerializeField] Mesh mesh;
    [SerializeField] Material material;

    void Awake()
    {
        var go = new GameObject("Cube" + mass);
        go.transform.position = transform.position;
        var cbrt = Mathf.Pow(mass, 1 / 3f);
        go.transform.localScale = Vector3.one * cbrt;
        var mesh = go.AddComponent<MeshFilter>();
        go.AddComponent<BoxCollider>();
        go.AddComponent<MeshRenderer>();
    }
}
