﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class LookAtTransform : MonoBehaviour
{
    [SerializeField] Transform root;
    [SerializeField] Transform target;
    [SerializeField] float rotateSpeed = 360f;
    [SerializeField] bool constrainX;
    [SerializeField] bool constrainY;
    [SerializeField] bool constrainZ;

    Vector3 desiredForward;

    void Update()
    {
        Execute();
    }

    void Execute()
    {
        if (!root || !target) return;

        var previousRot = root.rotation.eulerAngles;
        desiredForward = target.position - root.position;
        root.forward = Vector3.RotateTowards(root.forward, desiredForward, rotateSpeed * Time.deltaTime, 1);
        root.rotation = Quaternion.Euler(
            constrainX ? previousRot.x : root.rotation.eulerAngles.x,
            constrainY ? previousRot.y : root.rotation.eulerAngles.y,
            constrainZ ? previousRot.z : root.rotation.eulerAngles.z);
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
    }
}
