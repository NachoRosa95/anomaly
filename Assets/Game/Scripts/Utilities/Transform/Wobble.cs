﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct EditableProperty
{
    [HideInInspector] public float xFactor;
    [HideInInspector] public float startingValue;
    [HideInInspector] public float eval;
    [SerializeField] public float variance;
    [SerializeField] public float speed;
    [SerializeField] public AnimationCurve curve;
}

public class Wobble : MonoBehaviour
{
    [SerializeField] EditableProperty positionX;
    [SerializeField] EditableProperty positionY;
    [SerializeField] EditableProperty rotation;
    Text textComponent;

	public float GlobalSpeedMultiplier = 1;
	public float GlobalVarianceMultiplier = 1;

    private void Awake()
    {
        textComponent = GetComponent<Text>();
        positionX.startingValue = transform.localPosition.x;
        positionY.startingValue = transform.localPosition.y;
        rotation.startingValue = transform.localRotation.eulerAngles.z;
    }

    private void Update()
    {
		if(GlobalSpeedMultiplier == 0) return;
        transform.localPosition = new Vector2(Eval(ref positionX),Eval(ref positionY));
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, Eval(ref rotation));
    }

    private float Eval(ref EditableProperty property)
    {
        var step = property.speed * GlobalSpeedMultiplier * Time.deltaTime;
        property.xFactor = property.xFactor + step > 1 ? 0 : property.xFactor + step;
        property.eval = property.curve.Evaluate(property.xFactor);
        return Process(ref property);
    }

    private float Process(ref EditableProperty property)
    {
        var result = property.startingValue + (property.eval - 0.5f) * property.variance * GlobalVarianceMultiplier * 2;
        return result;
    }
}
