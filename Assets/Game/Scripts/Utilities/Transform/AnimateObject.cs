﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class AnimProperty
{
    [HideInInspector] public float xFactor;
    [HideInInspector] public float startingValue;
    [HideInInspector] public float eval;
    public float variance = 0;
    public float duration = 0;
    public AnimationCurve curve;

    // AnimationCurve GetTrigCurve(float intensity)
    // {
    //     return new AnimationCurve(new Keyframe(0, 0, intensity, intensity), new Keyframe(0.25f, 1f, 0, 0),
    //     new Keyframe(0.5f, 0, -intensity, -intensity), new Keyframe(0.75f, -1f, 0, 0), new Keyframe(1, 0f, intensity, intensity));
    // }
}

public class AnimateObject : MonoBehaviour
{
    public float SpeedMultiplier = 1;
    [SerializeField] AnimProperty speedMultiplier;
    [SerializeField] AnimProperty positionX;
    [SerializeField] AnimProperty positionY;
    [SerializeField] AnimProperty positionZ;
    [SerializeField] AnimProperty rotation;
    [SerializeField] AnimProperty fontSize;
    [SerializeField] AnimProperty scaleMultiplier;
    [SerializeField] AnimProperty scaleX;
    [SerializeField] AnimProperty scaleY;
    [SerializeField] AnimProperty scaleZ;
    RectTransform rectTransform;
    Rigidbody rb;
    Rigidbody2D rb2D;
    Text textComponent;

    private void Awake()
    {
        if (transform is RectTransform)
        {
            rectTransform = GetComponent<RectTransform>();
            positionX.startingValue = rectTransform.anchoredPosition.x;
            positionY.startingValue = rectTransform.anchoredPosition.y;
        }
        else
        {
            positionX.startingValue = transform.localPosition.x;
            positionY.startingValue = transform.localPosition.y;
            positionZ.startingValue = transform.localPosition.z;
        }
        scaleMultiplier.startingValue = 1;
        scaleX.startingValue = transform.localScale.x;
        scaleY.startingValue = transform.localScale.y;
        scaleZ.startingValue = transform.localScale.z;
        rotation.startingValue = transform.localRotation.eulerAngles.z;
        speedMultiplier.startingValue = SpeedMultiplier;
        textComponent = GetComponent<Text>();
        if (textComponent) fontSize.startingValue = textComponent.fontSize;
        rb = GetComponent<Rigidbody>();
        rb2D = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        if (!enabled) return;
        if (rb)
        {
            rb.MovePosition(new Vector3(Eval(positionX), Eval(positionY), Eval(positionZ)));
            rb.MoveRotation(Quaternion.Euler(transform.rotation.x, transform.rotation.y, Eval(rotation)));
        }
        else if (rb2D)
        {
            rb2D.MovePosition(new Vector3(Eval(positionX), Eval(positionY)));
            rb2D.MoveRotation(Eval(rotation));
        }
        else if (rectTransform)
        {
            rectTransform.anchoredPosition = new Vector2(Eval(positionX), Eval(positionY));
            rectTransform.localRotation = Quaternion.Euler(rectTransform.rotation.x, rectTransform.rotation.y, Eval(rotation));
        }
        else
        {
            transform.localPosition = new Vector3(Eval(positionX), Eval(positionY), Eval(positionZ));
            transform.localRotation = Quaternion.Euler(transform.localRotation.x, transform.localRotation.y, Eval(rotation));
        }
        transform.localScale = new Vector3(Eval(scaleX), Eval(scaleY), Eval(scaleZ)) * Eval(scaleMultiplier);
        if (textComponent) textComponent.fontSize = (int)Eval(fontSize);
        SpeedMultiplier = Eval(speedMultiplier);
    }

    private float Eval(AnimProperty property)
    {
        if (property.duration == 0) { property.xFactor = 0; return property.startingValue; }
        var step = 1 / property.duration * SpeedMultiplier * Time.fixedDeltaTime;
        // property.xFactor = property.xFactor + step > 1 ? (property.xFactor + step) % 1 : property.xFactor + step;
        property.xFactor = Mathf.MoveTowards(property.xFactor, Mathf.Sign(step) * float.MaxValue, step);
        // property.xFactor += step;
        // else if (property.xFactor > 1) property.xFactor %= 1;
        // else if (property.xFactor < 0) property.xFactor = 1 - property.xFactor % 1;
        property.eval = property.curve.Evaluate(property.xFactor);
        return Process(property);
    }

    private float Process(AnimProperty property)
    {
        var result = property.startingValue + (property.eval) * property.variance;
        return result;
    }
}
