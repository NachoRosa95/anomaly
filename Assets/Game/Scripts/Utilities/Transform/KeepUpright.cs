﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepUpright : MonoBehaviour
{
    void Update()
    {
        transform.LookAt(transform.position + transform.forward, Vector3.up);
    }
}
