﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformParenting : MonoBehaviour
{
    Collider platformCollider;

    // void OnTriggerStay(Collider col)
    // {
    //     if (!enabled) return;
    //     // Debug.Log(col);
    //     if (col.gameObject.tag != "Platform") return;
    //     target = col.gameObject;
    //     offset = target.transform.position - main.position;
    // }

    // void OnTriggerExit(Collider col)
    // {
    //     if (!enabled) return;
    //     Debug.Log("exit");
    //     if (col.gameObject.tag != "Platform") return;
    //     target = null;
    // }

    void Update()
    {
        if (!IsOnPlatform()) SetParent(null);
    }

    void OnCollisionStay(Collision collision)
    {
        if (!enabled) return;
        if (collision.gameObject.tag != "Platform") return;
        foreach (ContactPoint point in collision.contacts)
        {
            if (point.normal.y < 0.5) continue;
            SetParent(collision.collider);
        }
    }

    public void SetParent(Collider col)
    {
        transform.parent = col ? col.transform : null;
        platformCollider = col;
    }

    bool IsOnPlatform()
    {
        if (!platformCollider || Physics.GetIgnoreLayerCollision(platformCollider.gameObject.layer, gameObject.layer)) return false;
        var closestPoint = platformCollider.ClosestPoint(transform.position);
        var dist = Vector3.Distance(transform.position, platformCollider.ClosestPoint(transform.position));
        Debug.Log($"IsOnPlatform {platformCollider} {closestPoint} {dist}");
        if (dist > 0.1f) return false;
        return true;
    }

    // void OnCollisionExit(Collision collision)
    // {
    //     if (!enabled) return;
    //     if (collision.gameObject.tag != "Platform") return;
    //     transform.parent = null;
    // }
}
