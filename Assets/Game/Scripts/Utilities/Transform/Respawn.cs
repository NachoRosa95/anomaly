﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Respawn : MonoBehaviour
{
    [SerializeField] float OutOfBoundsY = -100;
    Rigidbody rb;
    // Vector3 startingPosition;
    // Quaternion startingRotation;

    GameObject cachedObject;
    Transform cachedParent;
    GameObject cacheStorage;

    public UnityEvent OnRespawn = new UnityEvent();

    void Awake()
    {
        // startingPosition = transform.position;
        // startingRotation = transform.rotation;
        // if (startingPosition.y < OutOfBoundsY)
        // {
        //     Debug.LogWarning("Warning: respawn position is out of bounds, disabling Respawn.cs", this);
        //     enabled = false;
        //     return;
        // }

        rb = GetComponent<Rigidbody>();

        cacheStorage = GameObject.Find("_CacheStorage") ?? new GameObject("_CacheStorage");
        gameObject.SetActive(false);
        cachedObject = Instantiate<GameObject>(gameObject);
        cachedObject.name = gameObject.name;
        cachedParent = transform.parent;
        cachedObject.transform.SetParent(cacheStorage.transform, true);
        gameObject.SetActive(true);
    }

    void Update()
    {
        if (transform.position.y < OutOfBoundsY) OnDeath();
    }

    public void SetSpawnPoint(Transform reference)
    {
        cachedObject.transform.position = reference.position;
        cachedObject.transform.rotation = reference.rotation;
    }

    void OnDeath()
    {
        if (!isActiveAndEnabled) return;
        if (rb) rb.velocity = Vector3.zero;
        cachedObject.transform.SetParent(cachedParent, false);
        cachedObject.SetActive(true);
        OnRespawn?.Invoke();
        gameObject.SetActive(false);
        Destroy(gameObject, 0.01f);
    }

    void OnDrawGizmos()
    {
        var origin = transform.position;
        origin.y = OutOfBoundsY;
        Gizmos.color = new Color(1, 0, 0, 0.2f);
        Gizmos.DrawCube(origin, Vector3.left * 100 + Vector3.forward * 100);
    }
}
