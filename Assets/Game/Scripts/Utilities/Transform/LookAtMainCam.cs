﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class LookAtMainCam : MonoBehaviour
{
    [SerializeField] bool constrainX;
    [SerializeField] bool constrainY;
    [SerializeField] bool constrainZ;
    [SerializeField] bool matchRotation;
    Camera mainCam;

    void Update()
    {
        if (mainCam == null) mainCam = Camera.main;
        Execute();
    }

    void Execute()
    {
        var previousRot = transform.rotation.eulerAngles;
        if (matchRotation)
        {
            transform.rotation = mainCam.transform.rotation;
        }
        else
        {
            transform.LookAt(mainCam.transform, mainCam.transform.up);
        }
        transform.rotation = Quaternion.Euler(
            constrainX ? previousRot.x : transform.rotation.eulerAngles.x,
            constrainY ? previousRot.y : transform.rotation.eulerAngles.y,
            constrainZ ? previousRot.z : transform.rotation.eulerAngles.z);
    }
}
