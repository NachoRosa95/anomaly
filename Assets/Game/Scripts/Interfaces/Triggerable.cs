﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Triggerable : MonoBehaviour
{
    [SerializeField] List<string> validTags;

    void Awake()
    {
        if (validTags.Count == 0) validTags.Add("Player");
    }

    void OnTriggerEnter(Collider other)
    {
        if (!IsValid(other.gameObject)) return;

        SendMessage("OnTriggeredEnter", other.gameObject, SendMessageOptions.DontRequireReceiver);
    }

    void OnTriggerExit(Collider other)
    {
        if (!IsValid(other.gameObject)) return;

        SendMessage("OnTriggeredExit", other.gameObject, SendMessageOptions.DontRequireReceiver);
    }

    void OnTriggerStay(Collider other)
    {
        if (!IsValid(other.gameObject)) return;

        SendMessage("OnTriggeredStay", other.gameObject, SendMessageOptions.DontRequireReceiver);
    }

    bool IsValid(GameObject gameObject)
    {
        return validTags.Contains(gameObject.tag);
    }

}
