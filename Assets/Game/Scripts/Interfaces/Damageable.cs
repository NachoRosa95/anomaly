﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damageable : MonoBehaviour
{
    GameObject damageDealer;
    public GameObject DamageDealer { get { return damageDealer; } set { damageDealer = value; } }

    HealthComponent health;
    public HealthComponent Health { get { return health; } set { health = value; } }

    [SerializeField] AudioSource audioSource;
    [SerializeField] float pitchVariation;
    private float initialPitch;

    private void Awake()
    {
        health = GetComponent<HealthComponent>();
        if (!audioSource) audioSource = GetComponent<AudioSource>();
        if (audioSource) initialPitch = audioSource.pitch;
    }

    public void Damage(float amount, GameObject damageDealer = null)
    {
        if (health != null)
        {
            health.Damage(amount, damageDealer);
            //Debug.Log("I, "+ transform.gameObject + " have taken " + amount + " damage from "+ damageDealer);
        }
        if (audioSource && audioSource.enabled)
        {
            audioSource.pitch = initialPitch + Random.Range(initialPitch - pitchVariation, initialPitch + pitchVariation);
            audioSource.Play();
        }
    }

}
