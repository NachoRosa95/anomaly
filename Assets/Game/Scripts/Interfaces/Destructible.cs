﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour
{
    void OnDeath()
    {
        //animation etc
        SendMessage("OnDestroyed", SendMessageOptions.DontRequireReceiver);
        Destroy(gameObject);
    }
}
