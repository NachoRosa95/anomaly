﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Interactable : MonoBehaviour
{
    [SerializeField] GameObject interactLabelPrefab;
    [SerializeField] Canvas canvas;
    [SerializeField] string buttonName = "Interact";
    [SerializeField] string labelText;
    [SerializeField] float cooldown = 1;
    GameObject interactLabel;
    bool onCooldown;

    void Awake()
    {
        if (canvas == null) canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
    }

    void OnDestroy()
    {
        if (interactLabel) interactLabel.SetActive(false);
    }

    void Update()
    {
        if (interactLabel == null || !interactLabel.activeSelf) return;


        if (Input.GetButtonDown(buttonName)) Interact();
    }

    void OnTriggeredStay()
    {
        if (onCooldown) return;
        if (interactLabel == null) interactLabel = CreateLabel();

        var text = "[" + buttonName + "] " + labelText;
        var textLabel = interactLabel.GetComponent<Text>();
        if (textLabel != null) textLabel.text = text;

        var textMesh = interactLabel.GetComponent<TextMeshProUGUI>();
        if (textMesh) textMesh.text = text;

        interactLabel.SetActive(true);
    }

    void OnTriggeredExit()
    {
        if (interactLabel == null) interactLabel = CreateLabel();


        interactLabel.SetActive(false);
    }

    GameObject CreateLabel()
    {
        var newLabel = Instantiate(interactLabelPrefab, canvas.transform);
        return newLabel;
    }

    void StopCooldown()
    {
        onCooldown = false;
    }

    void Interact()
    {
        onCooldown = true;
        Invoke("StopCooldown", cooldown);
        SendMessage("OnInteracted", SendMessageOptions.DontRequireReceiver);
        interactLabel.SetActive(false);
    }
}
