﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Highlightable : MonoBehaviour
{
    [SerializeField] protected GameObject gfx;
    [SerializeField] protected Projector projector;

    public bool Highlighted;

    void Awake()
    {
        if (projector) projector.orthographicSize = GetShadowScale();
    }

    protected void Highlight()
    {
        if (gfx)
        {
            gfx.SetActive(Highlighted);
            if (projector) projector.enabled = Highlighted;
        }
    }

    float GetShadowScale()
    {
        var meshes = GetComponentsInChildren<MeshRenderer>();
        var maxScale = 0f;
        foreach (MeshRenderer mesh in meshes)
        {
            var scale = mesh.transform.lossyScale.x;
            if (scale > maxScale) maxScale = scale;
        }
        return maxScale / 2;
    }
}
