﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collideable : MonoBehaviour
{
    [SerializeField] List<string> validTags;

    void Awake()
    {
        if (validTags.Count == 0)
        {
            validTags = new List<string>();
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (validTags.Count > 0 && !validTags.Contains(collision.gameObject.tag)) return;
        SendMessage("CollisionEntered", SendMessageOptions.DontRequireReceiver);
    }

    void OnCollisionExit(Collision collision)
    {
        if (validTags.Count > 0 && !validTags.Contains(collision.gameObject.tag)) return;
        SendMessage("CollisionExited", SendMessageOptions.DontRequireReceiver);
    }

    void OnCollisionStay(Collision collision)
    {
        if (validTags.Count > 0 && !validTags.Contains(collision.gameObject.tag)) return;
        SendMessage("CollisionStayed", SendMessageOptions.DontRequireReceiver);
    }
}
