﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class Haulable : Highlightable
{
    [SerializeField] GameObject body;
    [SerializeField] FadeMaterials fade;
    [SerializeField] PhysicMaterial selectedPhysics;
    [SerializeField] int noCollideLayer;
    [SerializeField] float selectedDrag = 10;
    [SerializeField] float selectedMass = 0.001f;

    public bool Hauling;
    public bool Throwable;

    List<MeshRenderer> bodyRenderers = new List<MeshRenderer>();
    Collider col;
    PhysicMaterial initPhysics;
    Rigidbody rb;

    int initLayer;
    bool wasHauling;
    float initDrag;
    float initMass;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        if (rb) 
        {
            initDrag = rb.drag;
            initMass = rb.mass;
        }
        if (body)
        {
            initLayer = body.layer;
            col = body.GetComponent<Collider>();
            initPhysics = col.material;
            bodyRenderers = body.GetComponentsInChildren<MeshRenderer>().ToList();
        }
    }

    void Update()
    {
        Highlight();

        if (Hauling && !wasHauling)
        {
            foreach (var rend in bodyRenderers) rend.enabled = false;
            fade.gameObject.SetActive(true);
            col.material = selectedPhysics;
            rb.drag = selectedDrag;
            rb.mass = selectedMass;
            fade.Execute(false);
            if (noCollideLayer != -1) body.layer = noCollideLayer;
        }
        if (!Hauling && wasHauling)
        {
            var rends = body.GetComponentsInChildren<MeshRenderer>();
            fade.Execute(true, ShowBody);
            body.layer = initLayer;
            col.material = initPhysics;
            rb.drag = initDrag;
            rb.mass = initMass;
        }
        wasHauling = Hauling;
    }

    void ShowBody()
    {
        foreach (var rend in bodyRenderers) rend.enabled = true;
        fade.gameObject.SetActive(false);
    }
}
