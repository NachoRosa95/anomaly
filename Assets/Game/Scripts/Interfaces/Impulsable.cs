﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Impulsable : Highlightable
{
    [SerializeField] TimeBody timeBody;

    void Update()
    {
        gfx.SetActive(timeBody.AppliedForce.magnitude != 0);
    }

    public void AddForce(Vector3 force)
    {
        timeBody.AppliedForce += force;
        gfx.transform.localScale = new Vector3(1, 1, timeBody.AppliedForce.magnitude);
        gfx.transform.forward = timeBody.AppliedForce;
    }

    public void ApplyForce()
    {
        timeBody.ApplyForce();
    }
}
