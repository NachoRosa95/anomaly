﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

public class FadeMaterials : MonoBehaviour
{
    [SerializeField] List<Material> primaryMaterials = new List<Material>();
    [SerializeField] List<Material> secondaryMaterials = new List<Material>();
    [SerializeField] AnimationCurve animationCurve;
    [SerializeField] float duration = 1;
    

    List<MeshRenderer> meshRenderers = new List<MeshRenderer>();
    Material targetMaterial;
    float t;
    bool reversing = false;
    // bool wasReversing = true;
    UnityAction callback;

    void Awake()
    {
        if (meshRenderers.Count == 0)
            meshRenderers = GetComponentsInChildren<MeshRenderer>().ToList();
    }

    [FoldoutGroup("Debug")]
    [Button]
    void Run()
    {
        Execute(true);
    }

    [FoldoutGroup("Debug")]
    [Button]
    void RunReverse()
    {
        Execute(false);
    }
    

    public void Execute(bool reverse, UnityAction callback = default)
    {
        this.callback = callback;
        // foreach (var rend in meshRenderers)
        // {
        //     if (!rend.enabled) rend.enabled = true;
        // }
        reversing = reverse;
        t = 0;
        StartCoroutine(FadeCoroutine());
    }

    public void End()
    {
        StopAllCoroutines();
        FadeMaterial(0);
    }

    private IEnumerator FadeCoroutine()
    {
        while (t < 1)
        {
            t += Time.deltaTime / duration;
            FadeMaterial(t, reversing);
            yield return 0;
        }
        callback?.Invoke();
    }

    public void FadeMaterial(float time, bool reverse = false)
    {
        if (reverse) time = 1 - time;
        var eval = animationCurve.Evaluate(time);
        foreach (var rend in meshRenderers)
        {
            var i = 0;
            foreach (var mat in rend.materials)
            {
                i++;
                // if (reverse != wasReversing)
                // {
                //     Debug.Log("Reverse changed: " + reverse + " " + wasReversing);
                //     if (reverse) StandardShaderUtils.ChangeRenderMode(mat, StandardShaderUtils.BlendMode.Fade);
                //     else StandardShaderUtils.ChangeRenderMode(mat, StandardShaderUtils.BlendMode.Opaque);
                // }
                var primaryMat = i < primaryMaterials.Count ? primaryMaterials[i] : primaryMaterials[primaryMaterials.Count - 1];
                var secondaryMat = i < secondaryMaterials.Count ? secondaryMaterials[i] : secondaryMaterials[secondaryMaterials.Count - 1];
                mat.Lerp(primaryMat, secondaryMat, eval);
            }
        }
        // wasReversing = reverse;
    }
}
