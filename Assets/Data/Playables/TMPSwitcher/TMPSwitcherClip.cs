using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class TMPSwitcherClip : PlayableAsset, ITimelineClipAsset
{
    public TMPSwitcherBehaviour template = new TMPSwitcherBehaviour ();

    public ClipCaps clipCaps
    {
        get { return ClipCaps.Blending; }
    }

    public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<TMPSwitcherBehaviour>.Create (graph, template);
        return playable;    }
}
