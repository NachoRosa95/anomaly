using UnityEditor;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

[CustomPropertyDrawer(typeof(TMPSwitcherBehaviour))]
public class TMPSwitcherDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {   
        int fieldCount = 20;
        return fieldCount * EditorGUIUtility.singleLineHeight;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty controlColorProp = property.FindPropertyRelative("controlColor"); ;
        SerializedProperty controlFontSizeProp = property.FindPropertyRelative("controlFontSize"); ;
        SerializedProperty controlTextProp = property.FindPropertyRelative("controlText"); ;

        SerializedProperty colorProp = property.FindPropertyRelative("color");
        SerializedProperty fontSizeProp = property.FindPropertyRelative("fontSize");
        SerializedProperty textProp = property.FindPropertyRelative("text");

        Rect singleFieldRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
        EditorGUI.PropertyField(singleFieldRect, controlColorProp);

        if (controlColorProp.boolValue)
        {
            singleFieldRect.y += EditorGUIUtility.singleLineHeight;
            EditorGUI.PropertyField(singleFieldRect, colorProp);
        }


        singleFieldRect.y += EditorGUIUtility.singleLineHeight;
        EditorGUI.PropertyField(singleFieldRect, controlFontSizeProp);

        if (controlFontSizeProp.boolValue)
        {
            singleFieldRect.y += EditorGUIUtility.singleLineHeight;
            EditorGUI.PropertyField(singleFieldRect, fontSizeProp);
        }


        singleFieldRect.y += EditorGUIUtility.singleLineHeight;
        EditorGUI.PropertyField(singleFieldRect, controlTextProp);

        if (controlTextProp.boolValue)
        {
            singleFieldRect.y += EditorGUIUtility.singleLineHeight;
            singleFieldRect.height = EditorGUIUtility.singleLineHeight * 4;
            EditorGUI.PropertyField(singleFieldRect, textProp);
        }
    }
}
