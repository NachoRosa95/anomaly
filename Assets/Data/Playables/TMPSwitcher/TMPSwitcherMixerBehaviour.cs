using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;
using TMPro;

public class TMPSwitcherMixerBehaviour : PlayableBehaviour
{
    Color m_DefaultColor;
    int m_DefaultFontSize;
    [TextArea(3, 10)]
    string m_DefaultText;

    bool m_ControlsColor;
    bool m_ControlsFontSize;
    bool m_ControlsText;

    TextMeshProUGUI m_TrackBinding;
    bool m_FirstFrameHappened;

    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        m_TrackBinding = playerData as TextMeshProUGUI;

        if (m_TrackBinding == null)
            return;

        if (!m_FirstFrameHappened)
        {
            m_DefaultColor = m_TrackBinding.color;
            m_DefaultFontSize = (int)m_TrackBinding.fontSize;
            m_DefaultText = m_TrackBinding.text;
            m_FirstFrameHappened = true;
        }

        m_ControlsColor = false;
        m_ControlsFontSize = false;
        m_ControlsText = false;

        int inputCount = playable.GetInputCount();

        Color blendedColor = Color.clear;
        float blendedFontSize = 0f;
        float totalWeight = 0f;
        float greatestWeight = 0f;
        int currentInputs = 0;

        for (int i = 0; i < inputCount; i++)
        {
            float inputWeight = playable.GetInputWeight(i);
            ScriptPlayable<TMPSwitcherBehaviour> inputPlayable = (ScriptPlayable<TMPSwitcherBehaviour>)playable.GetInput(i);
            TMPSwitcherBehaviour input = inputPlayable.GetBehaviour();
            if (input == null) continue;

            totalWeight += inputWeight;

            if (input.controlColor)
            {
                blendedColor += input.color * inputWeight;
            }

            if (input.controlFontSize)
            {
                blendedFontSize += input.fontSize * inputWeight;
            }

            if (inputWeight > greatestWeight)
            {
                if (input.controlText)
                {
                    var charCount = (int)(input.text.Length * inputWeight);
                    m_TrackBinding.text = input.text.Substring(0, charCount);
                }
                greatestWeight = inputWeight;

            }

            if (!Mathf.Approximately(inputWeight, 0f))
            {
                if (input.controlText) m_ControlsText = true;
                if (input.controlColor) m_ControlsColor = true;
                if (input.controlFontSize) m_ControlsFontSize = true;
                currentInputs++;
            }
        }

        if (m_ControlsColor) m_TrackBinding.color = blendedColor + m_DefaultColor * (1f - totalWeight);
        else m_TrackBinding.color = m_DefaultColor;

        if (m_ControlsFontSize) m_TrackBinding.fontSize = Mathf.RoundToInt(blendedFontSize + m_DefaultFontSize * (1f - totalWeight));
        else m_TrackBinding.fontSize = m_DefaultFontSize;

        if (currentInputs != 1 && 1f - totalWeight > greatestWeight)
        {
            m_TrackBinding.text = m_DefaultText;
        }
    }

    public override void OnGraphStop(Playable playable)
    {
        m_TrackBinding.color = m_DefaultColor;
        m_TrackBinding.fontSize = m_DefaultFontSize;
        m_TrackBinding.text = m_DefaultText;
        m_FirstFrameHappened = false;
        Debug.LogWarning("OnGraphStop");
    }
}
