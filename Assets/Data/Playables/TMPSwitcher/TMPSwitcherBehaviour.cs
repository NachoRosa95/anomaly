using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;

[Serializable]
public class TMPSwitcherBehaviour : PlayableBehaviour
{
    public bool controlColor = true;
    public bool controlFontSize = true;
    public bool controlText = true;

    public Color color = Color.black;
    public int fontSize = 10;
    [TextArea]
    public string text = "";
}
