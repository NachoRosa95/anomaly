using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class AnimatorControlClip : PlayableAsset, ITimelineClipAsset
{
    public AnimatorControlBehaviour template = new AnimatorControlBehaviour ();

    public ClipCaps clipCaps
    {
        get { return ClipCaps.None; }
    }

    public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<AnimatorControlBehaviour>.Create (graph, template);
        return playable;
    }
}
