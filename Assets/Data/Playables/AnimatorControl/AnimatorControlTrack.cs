using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[TrackColor(0.4431373f, 0.5254902f, 0.5647059f)]
[TrackClipType(typeof(AnimatorControlClip))]
[TrackBindingType(typeof(AnimatorController))]
public class AnimatorControlTrack : TrackAsset
{
    public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
    {
        return ScriptPlayable<AnimatorControlMixerBehaviour>.Create(graph, inputCount);
    }

//     public override void GatherProperties(PlayableDirector director, IPropertyCollector driver)
//     {
// #if UNITY_EDITOR
//         Animator trackBinding = director.GetGenericBinding(this) as Animator;
//         if (trackBinding == null)
//             return;
//         var so = new UnityEditor.SerializedObject(trackBinding);
//         var iter = so.GetIterator();
//         while (iter.NextVisible(true))
//         {
//             if (iter.hasVisibleChildren)
//                 continue;
//             driver.AddFromName<Animator>(trackBinding.gameObject, iter.propertyPath);
//         }
// #endif
//         base.GatherProperties(director, driver);
//     }
}
