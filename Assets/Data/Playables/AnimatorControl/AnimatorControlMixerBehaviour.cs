using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class AnimatorControlMixerBehaviour : PlayableBehaviour
{
    bool m_FirstFrameHappened;
    AnimatorControlBehaviour activeInput;

    // NOTE: This function is called at runtime and edit time.  Keep that in mind when setting the values of properties.
    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        AnimatorController trackBinding = playerData as AnimatorController;

        if (!trackBinding)
            return;

        // if (m_FirstFrameHappened) return;

        int inputCount = playable.GetInputCount();

        for (int i = 0; i < inputCount; i++)
        {
            float inputWeight = playable.GetInputWeight(i);
            ScriptPlayable<AnimatorControlBehaviour> inputPlayable = (ScriptPlayable<AnimatorControlBehaviour>)playable.GetInput(i);
            AnimatorControlBehaviour input = inputPlayable.GetBehaviour();

            // if (input.debug) Debug.Log($"Input weight: {inputWeight}", trackBinding);
            if (input == activeInput || inputWeight < 0.1f)
            {
                continue;
            }

            activeInput = input;
            // Use the above variables to process each frame of this playable.
            if (input.triggerName != string.Empty)
            {
                if (input.debug) Debug.Log($"Setting trigger {input.triggerName}", trackBinding);
                trackBinding.SetTrigger(input.triggerName);
            }
            if (input.boolName != string.Empty)
            {
                if (input.debug) Debug.Log($"Setting bool {input.boolName} to {input.boolValue}");
                trackBinding.SetBool(input.boolName, input.boolValue);
            }
            if (input.floatName != string.Empty)
            {
                if (input.debug) Debug.Log($"Setting float {input.floatName} to {input.floatValue}");
                trackBinding.SetFloat(input.floatName, input.floatValue);
            }
            if (input.intName != string.Empty)
            {
                if (input.debug) Debug.Log($"Setting float {input.intName} to {input.intValue}");
                trackBinding.SetInt(input.intName, input.intValue);
            }

        }
        m_FirstFrameHappened = true;
    }

    public override void OnPlayableDestroy(Playable playable)
    {
        m_FirstFrameHappened = false;
    }
}
