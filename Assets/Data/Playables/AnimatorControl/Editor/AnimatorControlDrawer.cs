using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(AnimatorControlBehaviour))]
public class AnimatorControlDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        int fieldCount = 20;
        return fieldCount * EditorGUIUtility.singleLineHeight;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty debugProp = property.FindPropertyRelative("debug");
        SerializedProperty setTriggerProp = property.FindPropertyRelative("triggerName");

        SerializedProperty setBoolProp = property.FindPropertyRelative("boolName");
        SerializedProperty boolValueProp = property.FindPropertyRelative("boolValue");

        SerializedProperty setFloatProp = property.FindPropertyRelative("floatName");
        SerializedProperty floatValueProp = property.FindPropertyRelative("floatValue");

        Rect singleFieldRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
        EditorGUI.PropertyField(singleFieldRect, debugProp);
        
        singleFieldRect.y += EditorGUIUtility.singleLineHeight;
        
        singleFieldRect.y += EditorGUIUtility.singleLineHeight;
        EditorGUI.PropertyField(singleFieldRect, setTriggerProp);

        singleFieldRect.y += EditorGUIUtility.singleLineHeight;

        singleFieldRect.y += EditorGUIUtility.singleLineHeight;
        EditorGUI.PropertyField(singleFieldRect, setBoolProp);

        singleFieldRect.y += EditorGUIUtility.singleLineHeight;
        EditorGUI.PropertyField(singleFieldRect, boolValueProp);

        singleFieldRect.y += EditorGUIUtility.singleLineHeight;

        singleFieldRect.y += EditorGUIUtility.singleLineHeight;
        EditorGUI.PropertyField(singleFieldRect, setFloatProp);

        singleFieldRect.y += EditorGUIUtility.singleLineHeight;
        EditorGUI.PropertyField(singleFieldRect, floatValueProp);
    }
}
