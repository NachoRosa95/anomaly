using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class AnimatorControlBehaviour : PlayableBehaviour
{
    public bool debug;

    public string triggerName;

    public string boolName;
    public bool boolValue;

    public string intName;
    public int intValue;

    public string floatName;
    public float floatValue;

    public override void OnPlayableCreate (Playable playable)
    {
        
    }
}
