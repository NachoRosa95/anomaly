using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(SelectUIElementBehaviour))]
public class SelectUIElementDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        int fieldCount = 1;
        return fieldCount * EditorGUIUtility.singleLineHeight;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty debugProp = property.FindPropertyRelative("debug");
        SerializedProperty indexProp = property.FindPropertyRelative("index");

        Rect singleFieldRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
        EditorGUI.PropertyField(singleFieldRect, debugProp);
        singleFieldRect.y += EditorGUIUtility.singleLineHeight;
        EditorGUI.PropertyField(singleFieldRect, indexProp);
    }
}
