using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class SelectUIElementMixerBehaviour : PlayableBehaviour
{
    private SelectUIElementBehaviour activeInput;

    // NOTE: This function is called at runtime and edit time.  Keep that in mind when setting the values of properties.
    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        SelectGateElement trackBinding = playerData as SelectGateElement;

        if (!trackBinding)
            return;

        int inputCount = playable.GetInputCount();

        for (int i = 0; i < inputCount; i++)
        {
            float inputWeight = playable.GetInputWeight(i);
            ScriptPlayable<SelectUIElementBehaviour> inputPlayable = (ScriptPlayable<SelectUIElementBehaviour>)playable.GetInput(i);
            SelectUIElementBehaviour input = inputPlayable.GetBehaviour();


            if (input == activeInput || inputWeight < 0.1f)
            {
                continue;
            }

            activeInput = input;
            if (input.debug) Debug.LogWarning((input == null ? "null" : input.ToString()) + " : " + (activeInput == null ? "null" : activeInput.ToString()));

            if (input.debug) Debug.LogWarning($"Running SelectUIElement.Execute({input.index}) on {trackBinding.name}", trackBinding);

            trackBinding.Execute(input.index);
        }
    }

    // public override void OnPlayableDestroy(Playable playable)
    // {
    //     int inputCount = playable.GetInputCount();

    //     for (int i = 0; i < inputCount; i++)
    //     {
    //         ScriptPlayable<SelectUIElementBehaviour> inputPlayable = (ScriptPlayable<SelectUIElementBehaviour>)playable.GetInput(i);
    //         SelectUIElementBehaviour input = inputPlayable.GetBehaviour();
    //         input.active = false;
    //     }
    // }
}
