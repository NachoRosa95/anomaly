using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class SelectUIElementClip : PlayableAsset, ITimelineClipAsset
{
    public SelectUIElementBehaviour template = new SelectUIElementBehaviour ();

    public ClipCaps clipCaps
    {
        get { return ClipCaps.None; }
    }

    public override Playable CreatePlayable (PlayableGraph graph, GameObject owner)
    {
        var playable = ScriptPlayable<SelectUIElementBehaviour>.Create (graph, template);
        SelectUIElementBehaviour clone = playable.GetBehaviour ();
        return playable;
    }
}
