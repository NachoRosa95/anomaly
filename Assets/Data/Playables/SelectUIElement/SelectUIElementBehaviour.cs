using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[Serializable]
public class SelectUIElementBehaviour : PlayableBehaviour
{
    public bool debug;
    public int index;

    public bool active;

    public override void OnPlayableCreate (Playable playable)
    {
        active = false;
    }

    public override void OnBehaviourPlay(Playable playable, FrameData info)
    {
        active = true;
    }

    public override void OnPlayableDestroy(Playable playable)
    {
        active = false;
    }
}
