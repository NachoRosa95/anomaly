using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

[TrackColor(0.2666667f, 0.482353f, 0.682353f)]
[TrackClipType(typeof(SelectUIElementClip))]
[TrackBindingType(typeof(SelectGateElement))]
public class SelectUIElementTrack : TrackAsset
{
    public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
    {
        return ScriptPlayable<SelectUIElementMixerBehaviour>.Create (graph, inputCount);
    }
}
