﻿Shader "Custom/TestShader"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Color (RGB) Alpha (A)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        _Position ("World Position", Vector) = (0,0,0,0)
        _Radius ("Radius", Range(0,50)) = 0.5
        _Softness("Softness", Range(0,100)) = 0
        _Alpha ("Alpha", Range(0,1)) = 0.5
    }
    SubShader
    {
        Tags { "Queue"="Transparent" "RenderType"="Transparent" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Lambert alpha


        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
            float3 worldPos;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        // Custom
        float4 _Position;
        float4 _Alpha;
        half _Radius;
        half _Softness;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutput o)
        {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            half grayscale = (c.r + c.g + c.b) * 0.333;
            fixed3 c_g = fixed3(grayscale,grayscale,grayscale);

            half dist = distance(_Position,IN.worldPos);
            half sum = saturate((dist - _Radius) / -_Softness);


            o.Albedo = c_g;
            // Metallic and smoothness come from slider variables
            o.Alpha = sum;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
