﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Noise-Simplex-Double" {
	Properties {
		_Contrast("Contrast", Float) = 2

		_Color("Color", Color) = (1,1,1,1)
		_Freq ("Frequency", Float) = 1
		_TimeScale ("Time Scale", Float) = 1
		_Speed ("Speed", Vector) = (0,0,0,0)
		[HDR] _EmissionColor("Emission Color", Color) = (0,0,0)
		_Intensity ("Emission Intensity", Float) = 1
		
		_Color2("Color 2", Color) = (1,1,1,1)
		_Freq2 ("Frequency 2", Float) = 1
		_TimeScale2 ("Time Scale 2", Float) = 1
		_Speed2 ("Speed 2", Vector) = (0,0,0,0)
		[HDR] _EmissionColor2("Emission Color 2", Color) = (0,0,0)
		_Intensity2 ("Emission Intensity 2", Float) = 1
	}

	SubShader { 
		Blend SrcAlpha OneMinusSrcAlpha
		Tags {"Queue" = "Transparent" "RenderType" = "Transparent" }
		Pass {
			CGPROGRAM
			
			#pragma target 3.0
			
			#pragma vertex vert
			#pragma fragment frag
			
			#include "noiseSimplex.cginc"
			
			struct v2f {
				float4 pos : SV_POSITION;
				float3 srcPos1 : TEXCOORD0;
				float3 srcPos2 : TEXCOORD1;
			};
			
			uniform float _Freq;

			float _Contrast;

			float4 _Speed;
			fixed4 _Color;
			fixed4 _EmissionColor;
			float _Intensity;
			float _TimeScale;
			
			uniform float _Freq2;
			
			float4 _Speed2;
			fixed4 _Color2;
			fixed4 _EmissionColor2;
			float _Intensity2;
			float _TimeScale2;
			
			
			v2f vert(float4 objPos : POSITION)
			{
				v2f o;

				o.pos =	UnityObjectToClipPos(objPos);
				
				o.srcPos1 = mul(unity_ObjectToWorld, objPos).xyz;
				o.srcPos1 *= _Freq;
				float time = _Time.y * _TimeScale;
				o.srcPos1.x += time * _Speed.x;
				o.srcPos1.y += time * _Speed.y;
				o.srcPos1.z += time * _Speed.z;
				
				o.srcPos2 = mul(unity_ObjectToWorld, objPos).xyz;
				o.srcPos2 *= _Freq2;
				time = _Time.y * _TimeScale2;
				o.srcPos2.x += time * _Speed2.x;
				o.srcPos2.y += time * _Speed2.y;
				o.srcPos2.z += time * _Speed2.z;


				return o;
			}
			
			float4 frag(v2f i) : COLOR
			{
				float time1 = _Time.y * _TimeScale;
				float time2 = _Time.y * _TimeScale2;
				float4 offset1 = float4(time1, time1, time1, time1);
				float ns = snoise(i.srcPos1+offset1) * _Contrast + 0.5f;
				float ns2 = snoise(i.srcPos2-ns*time2) * _Contrast + 0.5f;
				float4 output = float4(ns, ns, ns, ns) * _Color;
				output += float4(ns2, ns2, ns2, ns2) * _Color2;
				fixed4 emission1 = _EmissionColor * _Intensity;
				fixed4 emission2 = _EmissionColor2 * _Intensity2;
				emission1.a = output.a;
				emission2.a = output.a;
				output = (output + emission1 + emission2)/3;
				return output;
			}
			
			ENDCG
		}
	}

}