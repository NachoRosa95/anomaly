﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Noise-Simplex" {
Properties {
	_Color("Color", Color) = (1,1,1,1)
	_Freq ("Frequency", Float) = 1
	_Speed ("Speed", Vector) = (0,0,0,0)
	[HDR] _EmissionColor("Emission Color", Color) = (0,0,0)
	_Intensity ("Emission Intensity", Float) = 1

}

SubShader { 
	Blend SrcAlpha OneMinusSrcAlpha
	Tags {"Queue" = "Transparent" "RenderType" = "Transparent" }
	Pass {
		CGPROGRAM
		
		#pragma target 3.0
		
		#pragma vertex vert
		#pragma fragment frag
		
		#include "noiseSimplex.cginc"
		
		struct v2f {
			float4 pos : SV_POSITION;
			float3 srcPos : TEXCOORD0;
		};
		
		uniform float _Freq;

		float4 _Speed;
		fixed4 _Color;
		fixed4 _EmissionColor;
		float _Intensity;
		
		
		v2f vert(float4 objPos : POSITION)
		{
			v2f o;

			o.pos =	UnityObjectToClipPos(objPos);
			
			o.srcPos = mul(unity_ObjectToWorld, objPos).xyz;
			o.srcPos *= _Freq;
			o.srcPos.x += _Time.y * _Speed.x;
			o.srcPos.y += _Time.y * _Speed.y;
			o.srcPos.z += _Time.y * _Speed.z;


			return o;
		}
		
		float4 frag(v2f i) : COLOR
		{
			float ns = snoise(i.srcPos) / 2 + 0.5f;
			float4 output = float4(ns, ns, ns, ns) * _Color;
			fixed4 emission = _EmissionColor * _Intensity;
			emission.a = output.a;
			output = (output + emission)/2;
			return output;
		}
		
		ENDCG
	}
}

}