﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunCompute : MonoBehaviour
{

    private Vector2 cursorPos;

    // struct
    [System.Serializable]
    struct Particle
    {
        public Vector3 position;
        public Vector3 velocity;
        public Vector3 seed;
        public float life;
        public float density;
    }

    /// <summary>
	/// Size in octet of the Particle struct.
    /// since float = 4 bytes...
    /// 4 floats = 16 bytes
	/// </summary>
	//private const int SIZE_PARTICLE = 24;
    private const int SIZE_PARTICLE = 44; // since property "life" is added...

    /// <summary>
    /// Number of Particle created in the system.
    /// </summary>
    [Range(1000, 100000)] public int particleCount = 50000;

    /// <summary>
    /// Material used to draw the Particle on screen.
    /// </summary>
    public Material material;

    /// <summary>
    /// Compute shader used to update the Particles.
    /// </summary>
    public ComputeShader computeShader;

    /// <summary>
    /// Scale of initial particle cloud.
    /// </summary>
    public float scale = 1;

    /// <summary>
    /// e
    /// </summary>
    [Range(0.00001f, 1f)]
    public float e = 0.1f;

    /// <summary>
    /// Max particle lifetime
    /// </summary>
    public float lifeTime = 5f;

    /// <summary>
    /// Max particle lifetime
    /// </summary>
    public float maxDist = 10f;

    /// <summary>
    /// Max particle lifetime
    /// </summary>
    public float densityDist = 10f;

    /// <summary>
    /// Gravity scale
    /// </summary>
    public float suctionScale = 1f;

    /// <summary>
    /// Gravity scale
    /// </summary>
    public float speed = 1f;

    /// <summary>
    /// Gravity scale
    /// </summary>
    public float gravScale = 2f;

    /// <summary>
    /// Id of the kernel used.
    /// /// </summary>
    private int mComputeShaderKernelID;

    /// <summary>
    /// Buffer holding the Particles.
    /// </summary>
    ComputeBuffer particleBuffer;

    /// <summary>
    /// Number of particle per warp.
    /// </summary>
    private const int WARP_SIZE = 256; // TODO?

    /// <summary>
    /// Number of warp needed.
    /// </summary>
    private int mWarpCount; // TODO?

    //public ComputeShader shader;

    private Collider areaCollider;

    // Use this for initialization
    void Start()
    {
        InitComputeShader();
        areaCollider = GetComponent<Collider>();
    }

    void InitComputeShader()
    {
        mWarpCount = Mathf.CeilToInt((float)particleCount / WARP_SIZE);

        // initialize the particles
        Particle[] particleArray = new Particle[particleCount];

        for (int i = 0; i < particleCount; i++)
        {
            var xyz = Random.insideUnitSphere * scale;
            // float x = Random.value * 2 - 1.0f;
            // float y = Random.value * 2 - 1.0f;
            // float z = Random.value * 2 - 1.0f;
            // Vector3 xyz = new Vector3(x, y, z);
            // xyz.Normalize();
            // xyz *= Random.value;
            // xyz *= 0.5f;


            particleArray[i].position.x = xyz.x;
            particleArray[i].position.y = xyz.y;
            particleArray[i].position.z = xyz.z;
            particleArray[i].position += transform.position;

            particleArray[i].velocity.x = -xyz.z * Random.value;
            particleArray[i].velocity.y = xyz.x * Random.value;
            particleArray[i].velocity.z = xyz.y * Random.value;

            // Initial life value
            particleArray[i].life = Random.value * lifeTime + 1.0f;

            // Seed for random calculations
            particleArray[i].seed = new Vector3(Random.Range(0f,1f),Random.Range(0f,1f),Random.Range(0f,1f));
            Debug.Log(particleArray[i].seed);
        }

        // create compute buffer
        particleBuffer = new ComputeBuffer(particleCount, SIZE_PARTICLE);

        particleBuffer.SetData(particleArray);

        // find the id of the kernel
        mComputeShaderKernelID = computeShader.FindKernel("CSParticle");

        // bind the compute buffer to the shader and the compute shader
        computeShader.SetBuffer(mComputeShaderKernelID, "particleBuffer", particleBuffer);
        material.SetBuffer("particleBuffer", particleBuffer);
    }

    void OnRenderObject()
    {
        var filter = GetComponent<MeshFilter>();
        material.SetPass(0);
        Graphics.DrawProceduralNow(MeshTopology.Points, 32, (int)(particleCount));
    }

    void OnDestroy()
    {
        if (particleBuffer != null)
            particleBuffer.Release();
    }

    // // Update is called once per frame
    // void Update()
    // {
    //     // Send datas to the compute shader
    //     material.SetFloat("lifeTime", lifeTime);
    //     material.SetFloat("maxDist", maxDist);
    //     computeShader.SetFloat("deltaTime", Time.deltaTime);
    //     // computeShader.SetFloat("e", e);
    //     computeShader.SetFloat("lifeTime", lifeTime);

    //     material.SetFloat("centerX", transform.position.x);
    //     material.SetFloat("centerY", transform.position.y);
    //     material.SetFloat("centerZ", transform.position.z);

    //     computeShader.SetFloat("centerX", transform.position.x);
    //     computeShader.SetFloat("centerY", transform.position.y);
    //     computeShader.SetFloat("centerZ", transform.position.z);

    //     computeShader.SetFloat("gravScale", gravScale);

    //     float[] mousePosition2D = { cursorPos.x, cursorPos.y };
    //     computeShader.SetFloats("mousePos", mousePosition2D);

    //     // Update the Particles
    //     computeShader.Dispatch(mComputeShaderKernelID, mWarpCount, 1, 1);
    // }

    // Update is called once per frame
    void Update()
    {

        float[] mousePosition2D = { cursorPos.x, cursorPos.y };
        float[] position = { transform.position.x, transform.position.y, transform.position.z };

        // Send datas to the compute shader
        material.SetFloat("lifeTime", lifeTime);
        computeShader.SetFloat("scale", scale);
        computeShader.SetFloat("gravScale", gravScale);
        computeShader.SetFloat("suctionScale", suctionScale);
        computeShader.SetFloat("deltaTime", Time.deltaTime);
        computeShader.SetFloats("mousePosition", mousePosition2D);
        computeShader.SetFloats("position", position);
        computeShader.SetFloat("lifeTime", lifeTime);
        computeShader.SetFloat("speed", speed);
        computeShader.SetFloat("densityDist", densityDist);
        computeShader.SetFloat("e", e);

        var min = areaCollider.bounds.min;
        var max = areaCollider.bounds.max;
        float[] mins = { min.x, min.y, min.z };
        float[] maxes = { max.x, max.y, max.z };
        Debug.Log(min);
        Debug.Log(max);
        computeShader.SetFloats("areaMin", mins);
        computeShader.SetFloats("areaMax", maxes);

        // Update the Particles
        computeShader.Dispatch(mComputeShaderKernelID, mWarpCount, 1, 1);
    }

    void OnGUI()
    {
        Vector3 p = new Vector3();
        Camera c = Camera.main;
        Event e = Event.current;
        Vector2 mousePos = new Vector2();

        // Get the mouse position from Event.
        // Note that the y position from Event is inverted.
        mousePos.x = e.mousePosition.x;
        mousePos.y = c.pixelHeight - e.mousePosition.y;

        p = c.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, c.nearClipPlane + 14));// z = 3.

        cursorPos.x = p.x;
        cursorPos.y = p.y;
        /*
        GUILayout.BeginArea(new Rect(20, 20, 250, 120));
        GUILayout.Label("Screen pixels: " + c.pixelWidth + ":" + c.pixelHeight);
        GUILayout.Label("Mouse position: " + mousePos);
        GUILayout.Label("World position: " + p.ToString("F3"));
        GUILayout.EndArea();
        */
    }
}