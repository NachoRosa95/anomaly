﻿Shader "Custom/Particle" {

	Properties
	{
        _Color ("Color", Color) = (1,1,1,1)
	}

	SubShader {
		Pass {
		Tags {"Queue" = "Transparent" "RenderType" = "Transparent" }
			LOD 200
			Blend SrcAlpha one

			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			// Use shader model 3.0 target, to get nicer looking lighting
			#pragma target 5.0

			struct Particle{
				float3 position;
				float3 velocity;
				float3 seed;
				float life;
				float density;
			};
			
			struct PS_INPUT{
				float4 position : SV_POSITION;
				float4 color : COLOR;
				float life : LIFE;
			};
			// particles' data
			StructuredBuffer<Particle> particleBuffer;
			float lifeTime;
			float maxDist;

			float centerX;
			float centerY;
			float centerZ;
            uniform float4 _Color;
			

			PS_INPUT vert(uint vertex_id : SV_VertexID, uint instance_id : SV_InstanceID)
			{
				PS_INPUT o = (PS_INPUT)0;


				// Position
				o.position = UnityObjectToClipPos(float4(particleBuffer[instance_id].position, 1.0f));

				// Color
				// float life = particleBuffer[instance_id].life;
				// float lerpVal = life / lifeTime;

				float density = particleBuffer[instance_id].density;

				// float3 gravityCenter = float3(centerX, centerY, centerZ);
				// float dist = distance(gravityCenter, particleBuffer[instance_id].position);
				// float lerpVal = clamp(1/dist * maxDist, 0, 1);
				float4 color = _Color;
				color.a *= density;

				o.color = color;

				return o;
			}

			float4 frag(PS_INPUT i) : COLOR
			{
				return i.color;
			}


			ENDCG
		}
	}
}