%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: MaestroTop
  m_Mask: 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 1
  - m_Path: Armature/Bone.center.006
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center
    m_Weight: 0
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.center.004
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.center.004/Bone.center.002
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.center.004/Bone.center.002/Bone.center.003
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.center.004/Bone.center.002/Bone.center.003/Bone.center.005
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.center.004/Bone.center.002/Bone.center.003/Bone.center.005/Bone.center.005_end
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8/Bone.L.009
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8/Bone.L.009/Bone.L.010
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8/Bone.L.009/Bone.L.010/Bone.L.011
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8/Bone.L.009/Bone.L.010/Bone.L.011/Bone.L.012
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8/Bone.L.009/Bone.L.010/Bone.L.011/Bone.L.012/Bone.L.022
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8/Bone.L.009/Bone.L.010/Bone.L.011/Bone.L.012/Bone.L.022/Bone.L.017
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8/Bone.L.009/Bone.L.010/Bone.L.011/Bone.L.012/Bone.L.022/Bone.L.017/Bone.L.017_end
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8/Bone.L.009/Bone.L.010/Bone.L.011/Bone.L.013
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8/Bone.L.009/Bone.L.010/Bone.L.011/Bone.L.013/Bone.L.023
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8/Bone.L.009/Bone.L.010/Bone.L.011/Bone.L.013/Bone.L.023/Bone.L.018
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8/Bone.L.009/Bone.L.010/Bone.L.011/Bone.L.013/Bone.L.023/Bone.L.018/Bone.L.018_end
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8/Bone.L.009/Bone.L.010/Bone.L.011/Bone.L.014
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8/Bone.L.009/Bone.L.010/Bone.L.011/Bone.L.014/Bone.L.024
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8/Bone.L.009/Bone.L.010/Bone.L.011/Bone.L.014/Bone.L.024/Bone.L.019
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8/Bone.L.009/Bone.L.010/Bone.L.011/Bone.L.014/Bone.L.024/Bone.L.019/Bone.L.019_end
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8/Bone.L.009/Bone.L.010/Bone.L.011/Bone.L.015
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8/Bone.L.009/Bone.L.010/Bone.L.011/Bone.L.015/Bone.L.025
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8/Bone.L.009/Bone.L.010/Bone.L.011/Bone.L.015/Bone.L.025/Bone.L.020
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8/Bone.L.009/Bone.L.010/Bone.L.011/Bone.L.015/Bone.L.025/Bone.L.020/Bone.L.020_end
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8/Bone.L.009/Bone.L.010/Bone.L.011/Bone.L.016
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8/Bone.L.009/Bone.L.010/Bone.L.011/Bone.L.016/Bone.L.021
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.L.8/Bone.L.009/Bone.L.010/Bone.L.011/Bone.L.016/Bone.L.021/Bone.L.021_end
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8/Bone.R.009
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8/Bone.R.009/Bone.R.010
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8/Bone.R.009/Bone.R.010/Bone.R.011
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8/Bone.R.009/Bone.R.010/Bone.R.011/Bone.R.012
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8/Bone.R.009/Bone.R.010/Bone.R.011/Bone.R.012/Bone.R.022
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8/Bone.R.009/Bone.R.010/Bone.R.011/Bone.R.012/Bone.R.022/Bone.R.017
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8/Bone.R.009/Bone.R.010/Bone.R.011/Bone.R.012/Bone.R.022/Bone.R.017/Bone.R.017_end
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8/Bone.R.009/Bone.R.010/Bone.R.011/Bone.R.013
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8/Bone.R.009/Bone.R.010/Bone.R.011/Bone.R.013/Bone.R.023
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8/Bone.R.009/Bone.R.010/Bone.R.011/Bone.R.013/Bone.R.023/Bone.R.018
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8/Bone.R.009/Bone.R.010/Bone.R.011/Bone.R.013/Bone.R.023/Bone.R.018/Bone.R.018_end
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8/Bone.R.009/Bone.R.010/Bone.R.011/Bone.R.014
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8/Bone.R.009/Bone.R.010/Bone.R.011/Bone.R.014/Bone.R.024
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8/Bone.R.009/Bone.R.010/Bone.R.011/Bone.R.014/Bone.R.024/Bone.R.019
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8/Bone.R.009/Bone.R.010/Bone.R.011/Bone.R.014/Bone.R.024/Bone.R.019/Bone.R.019_end
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8/Bone.R.009/Bone.R.010/Bone.R.011/Bone.R.015
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8/Bone.R.009/Bone.R.010/Bone.R.011/Bone.R.015/Bone.R.025
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8/Bone.R.009/Bone.R.010/Bone.R.011/Bone.R.015/Bone.R.025/Bone.R.020
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8/Bone.R.009/Bone.R.010/Bone.R.011/Bone.R.015/Bone.R.025/Bone.R.020/Bone.R.020_end
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8/Bone.R.009/Bone.R.010/Bone.R.011/Bone.R.016
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8/Bone.R.009/Bone.R.010/Bone.R.011/Bone.R.016/Bone.R.021
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.center/Bone.center.001/Bone.R.8/Bone.R.009/Bone.R.010/Bone.R.011/Bone.R.016/Bone.R.021/Bone.R.021_end
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.L.002
    m_Weight: 0
  - m_Path: Armature/Bone.center.006/Bone.L.002/Bone.L.003
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.L.002/Bone.L.003/Bone.L
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.L.002/Bone.L.003/Bone.L/Bone.L.001
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.L.002/Bone.L.003/Bone.L/Bone.L.001/Bone.L.001_end
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.R.002
    m_Weight: 0
  - m_Path: Armature/Bone.center.006/Bone.R.002/Bone.R.003
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.R.002/Bone.R.003/Bone.R
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.R.002/Bone.R.003/Bone.R/Bone.R.001
    m_Weight: 1
  - m_Path: Armature/Bone.center.006/Bone.R.002/Bone.R.003/Bone.R/Bone.R.001/Bone.R.001_end
    m_Weight: 1
  - m_Path: master
    m_Weight: 1
  - m_Path: masterHat
    m_Weight: 1
