﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Transform))]
[CanEditMultipleObjects]
public class TransformEditorExtension : TransformInspector
{
    // void OnGUI()
    // {
    //     Debug.Log("Transform Editor");
    // }
}
